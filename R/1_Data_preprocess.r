##____________________________________________________________________________________________________________________________________#
## Data_complete load ##
##____________________________________________________________________________________________________________________________________#

dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data'
od <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R'
setwd(wd)

# import data
dat <- read.csv(file.path(dd, 'data_complete.csv'))
nrow(dat) #1134

#____________________________________________________________________________________________________________________________________#
# Load packages

library(usethis)
library(devtools)

##____________________________________________________________________________________________________________________________________#
## Check & restructure data ##
##____________________________________________________________________________________________________________________________________#

# Rename variables
dat$vol <- dat$Original_volume_Main.x
dat$multif <- dat$Mutifocality
dat$left <- dat$Left_laterality_Main
dat$right <- dat$Right_laterality_Main
dat$RI <- dat$ResectionIndex_Main
dat$billat <- dat$Midline_crossing_Main
dat$R_Par <- dat$MNI_parietal_right_Main
dat$R_temp <- dat$MNI_temporal_right_Main
dat$R_Occ <- dat$MNI_occipital_right_Main
dat$R_Front <- dat$MNI_frontal_right_Main
dat$L_Par <- dat$MNI_parietal_left_Main
dat$L_Temp <- dat$MNI_temporal_left_Main
dat$L_Occ <- dat$MNI_occipital_left_Main
dat$L_Front <- dat$MNI_frontal_left_Main
dat$visual <- dat$Schaefer7_visual_Main
dat$somatomotor <- dat$Schaefer7_somatomotor_Main
dat$dorsalAttention <- dat$Schaefer7_dorsalAttention_Main
dat$salienceVentralAttention <- dat$Schaefer7_salienceVentralAttention_Main
dat$limbic <- dat$Schaefer7_limbic_Main
dat$frontoparietalControl <- dat$Schaefer7_frontoparietalControl_Main
dat$default <- dat$Schaefer7_default_Main
dat$CST_R <- dat$overlap_Cortico_Spinal_Right_Main
dat$CST_L <- dat$overlap_Cortico_Spinal_Left_Main
dat$Arc_R <- dat$overlap_Arcuate_Right_Main
dat$Arc_L <- dat$overlap_Arcuate_Left_Main
dat$Thalamus <- dat$MNI_thalamus_left_Main + 
                dat$MNI_thalamus_right_Main
dat$PUT <- dat$MNI_putamen_left_Main + 
          dat$MNI_putamen_right_Main
dat$GP <- dat$MNI_globus_pallidus._left_Main + 
          dat$MNI_globus_pallidus._right_Main
dat$PrG <- dat$Harvard.Oxford_PrG_Main
dat$Taal_Cort <- dat$Harvard.Oxford_SmGa_Main + #supramarginalis ant
                    dat$Harvard.Oxford_SmGp_Main + #supramarginalis post 
                    dat$Harvard.Oxford_AG_Main + #angularis
                    dat$Harvard.Oxford_STGp_Main + #superior temporal post
                    dat$Harvard.Oxford_IFGpt_Main + #inferior frontal triangul
                    dat$Harvard.Oxford_IFGpo_Main #inferior frontal opercularis
dat$CST <- dat$overlap_Cortico_Spinal_Left_Main + 
          dat$overlap_Cortico_Spinal_Right_Main
dat$Taal_SubC <- dat$overlap_Anterior_Segment_Left_Main + 
                dat$overlap_Posterior_Segment_Left_Main + 
                dat$overlap_Long_Segment_Left_Main + 
                dat$overlap_Arcuate_Left_Main +
                dat$overlap_Inferior_Occipito_Frontal_Fasciculus_Left_Main

# Create variable according to Sawaya classification
dat$saw <- NULL
#SAWAYA 1
dat$saw [dat$Harvard.Oxford_FP_Main > 0] <- 1
dat$saw [dat$Harvard.Oxford_TP_Main > 0] <- 1      
# only right side
dat$saw [dat$Harvard.Oxford_SPL_Main > 0 |
          dat$left < 50] <- 1
dat$saw [dat$Harvard.Oxford_SmGa_Main > 0 |
          dat$left < 50] <- 1
dat$saw [dat$Harvard.Oxford_SmGp_Main > 0 |
          dat$left < 50] <- 1
dat$saw [dat$Harvard.Oxford_AG_Main > 0 |
          dat$left < 50] <- 1
dat$saw [dat$Harvard.Oxford_LOCs_Main > 0 |
          dat$left < 50] <- 1
dat$saw [dat$Harvard.Oxford_LOCi_Main > 0 |
          dat$left < 50] <- 1
dat$saw [dat$Harvard.Oxford_OcP_Main > 0 |
          dat$left < 50]<- 1
#SAWAYA 2
dat$saw [dat$Harvard.Oxford_SMC_Main > 0] <- 2
dat$saw [dat$Harvard.Oxford_PcG_Main > 0] <- 2
dat$saw [dat$Harvard.Oxford_LG_Main > 0] <- 2
dat$saw [dat$Harvard.Oxford_SccC_Main > 0] <- 2
# only left side
dat$saw [dat$Harvard.Oxford_STGa_Main > 0 |
           dat$left > 50] <- 2
dat$saw [dat$Harvard.Oxford_MTGa_Main > 0 |
          dat$left > 50] <- 2
dat$saw [dat$Harvard.Oxford_MTGp_Main > 0 |
          dat$left > 50] <- 2
dat$saw [dat$Harvard.Oxford_MTGtp_Main > 0 |
          dat$left > 50] <- 2
dat$saw [dat$Harvard.Oxford_SmGp_Main > 0 |
          dat$left > 50] <- 2
dat$saw [dat$Harvard.Oxford_FOpC_Main > 0 |
          dat$left > 50] <- 2
dat$saw [dat$Harvard.Oxford_POpC_Main > 0 |
          dat$left > 50] <- 2
dat$saw [dat$Harvard.Oxford_PT_Main > 0 |
          dat$left > 50] <- 2
#SAWAYA 3
dat$saw [dat$Harvard.Oxford_PrG_Main > 0] <- 3
dat$saw [dat$Harvard.Oxford_PoG_Main > 0] <- 3
dat$saw [dat$Harvard.Oxford_IcC_Main > 0] <- 3
dat$saw [dat$Harvard.Oxford_IFGpt_Main > 0] <- 3
dat$saw [dat$Harvard.Oxford_IFGpo_Main > 0] <- 3
dat$saw [dat$Harvard.Oxford_STGp_Main > 0] <- 3
dat$saw [dat$Harvard.Oxford_SmGa_Main > 0] <- 3
dat$saw [dat$overlap_Corpus_Callosum_Main > 0] <- 3
dat$saw [dat$overlap_Internal_Capsule_Main > 0] <- 3
dat$saw [dat$MNI_thalamus_left_Main > 0] <- 3
dat$saw [dat$MNI_thalamus_right_Main > 0] <- 3
dat$saw [dat$MNI_caudate_left_Main > 0] <- 3
dat$saw [dat$MNI_caudate_right_Main > 0] <- 3
dat$saw [dat$MNI_putamen_left_Main > 0] <- 3
dat$saw [dat$MNI_putamen_right_Main > 0] <- 3
dat$saw [dat$MNI_globus_pallidus._right_Main > 0] <- 3
dat$saw [dat$MNI_globus_pallidus._left_Main > 0] <- 3


# change data class
dat$SurgeryExtend.x <- as.factor(dat$SurgeryExtend.x)
dat$GenderV2.x <- as.factor(dat$GenderV2.x)
dat$KPSpre.x <- as.numeric(dat$KPSpre.x)
dat$group.x <- as.factor(dat$group.x)
dat$vol <- as.numeric(dat$vol)
dat$multif <- as.logical(dat$multif)
dat$billat <- as.logical(dat$billat)
dat$saw <- as.factor(dat$saw)
#class(dat$age.x) #numeric
#class(dat$ASA.x) #integer
#class(dat$ResVol) #numeric
#class(dat$left) #numeric
#class(dat$RI) #numeric

##_____________________________________________________________________________________________________________________
## Rescore data ##
##_____________________________________________________________________________________________________________________
library(dplyr)

# ASA.x 0-4 -> 1-5 
dat$ASA.x [dat$ASA.x == 4] <- 5
dat$ASA.x [dat$ASA.x == 3] <- 4
dat$ASA.x [dat$ASA.x == 2] <- 3
dat$ASA.x [dat$ASA.x == 1] <- 2
dat$ASA.x [dat$ASA.x == 0] <- 1
table(dat$ASA.x)  #1   2   3   4   5 
                   #45  63  96  87  0 

# Grouped ASA.x: 1-2 & 3-5
dat$ASA.x [dat$ASA.x == "5" |
         dat$ASA.x == "4" |
         dat$ASA.x == "3"] <- "High"
dat$ASA.x [dat$ASA.x == "2" |
         dat$ASA.x == "1"] <- "Low"
dat$ASA.x <- as.factor(dat$ASA.x)
#unique(dat$ASA.x): High, Low

# KPS change
dat$dKPS <- dat$KPSpos.x - dat$KPSpre.x

# survival
dat$surv <- dat$surv.x

# death observed
dat$death <- dat$DeathObserved.x

# KPS grouping
    # 100-70
    # 50-60
    # 10-40
dat$KPSpre.x [dat$KPSpre.x >= 7] <- ">=70"
dat$KPSpre.x [dat$KPSpre.x == 6 |
            dat$KPSpre.x == 5] <- "50-70"
dat$KPSpre.x [dat$KPSpre.x == 4 |
            dat$KPSpre.x == 3 |
            dat$KPSpre.x == 2 |
            dat$KPSpre.x == 1] <- "<50"
dat$KPSpre.x <- as.factor(dat$KPSpre.x)
# unique(dat$KPSpre.x) <50 >=70 50-70


# dealing with missing values
dat$GenderV2.x [dat$GenderV2.x == ""] <- NA
dat$RI [dat$RI == "-1"] <- NA
dat$left [dat$left == "-1"] <- NA
dat$R_Par [dat$R_Par == "-1"] <- NA
dat$R_temp [dat$R_temp == "-1"] <- NA
dat$R_Occ [dat$R_Occ == "-1"] <- NA
dat$R_Front [dat$R_Front == "-1"] <- NA
dat$L_Par [dat$L_Par == "-1"] <- NA
dat$L_Temp [dat$L_Temp == "-1"] <- NA
dat$L_Occ [dat$L_Occ == "-1"] <- NA
dat$L_Front [dat$L_Front == "-1"] <- NA
dat$visual [dat$visual == "-1"] <- NA
dat$somatomotor [dat$somatomotor == "-1"] <- NA
dat$dorsalAttention [dat$dorsalAttention == "-1"] <- NA
dat$salienceVentralAttention [dat$salienceVentralAttention == "-1"] <- NA
dat$limbic [dat$limbic == "-1"] <- NA
dat$frontoparietalControl [dat$frontoparietalControl == "-1"] <- NA
dat$default [dat$default == "-1"] <- NA
dat$CST_R [dat$CST_R == "-1"] <- NA
dat$CST_L [dat$CST_L == "-1"] <- NA
dat$Arc_R [dat$Arc_R == "-1"] <- NA
dat$Arc_L [dat$Arc_L == "-1"] <- NA
dat$Thalamus [dat$Thalamus == "-2"] <- NA
dat$PUT [dat$PUT == "-2"] <- NA
dat$GP [dat$GP == "-2"] <- NA
dat$PrG [dat$PrG == "-1"] <- NA
dat$Taal_Cort [dat$Taal_Cort == "-6"] <- NA
dat$CST [dat$CST == "-2"] <- NA
dat$Taal_SubC [dat$Taal_SubC == "-5"] <- NA

# binarize laterality
dat$lat <- dat$left
dat$lat [dat$lat < 50] <- "R"
dat$lat [dat$lat >50 & dat$lat != "R"] <- "L"
dat$lat [dat$lat == 100] <- "L"
dat$lat <- as.factor(dat$lat)

# remove right sided tumors from language area's
dat$Taal_Cort [dat$lat == "R"] <- 0

# Change magnitude variables
# age to per decade
dat$age.x <- dat$age.x /10
# volume to dL ipv mL
dat$vol <- dat$vol/10
# RI to steps of 10%
dat$RI <- dat$RI * 10
# change to per 10% of overlap oftotal tumor volume
dat$R_Par <- dat$R_Par / 10
dat$R_temp <- dat$R_temp / 10
dat$R_Occ <- dat$R_Occ / 10
dat$R_Front <- dat$R_Front / 10
dat$L_Par <- dat$L_Par / 10
dat$L_Temp <- dat$L_Temp / 10
dat$L_Occ <- dat$L_Occ / 10
dat$L_Front <- dat$L_Front / 10
dat$visual <- dat$visual / 10
dat$somatomotor <- dat$somatomotor / 10
dat$dorsalAttention <- dat$dorsalAttention / 10
dat$salienceVentralAttention <- dat$salienceVentralAttention / 10
dat$limbic <- dat$limbic / 10
dat$frontoparietalControl <- dat$frontoparietalControl / 10
dat$default <- dat$default / 10
dat$PrG <- dat$PrG /10
dat$Thalamus <- dat$Thalamus / 10
dat$PUT <- dat$PUT / 10
dat$GP <- dat$GP / 10
dat$Taal_Cort <- dat$Taal_Cort / 10
# White matter tract to per 10% of overlap
dat$CST <- round(dat$CST * 10, digit=2)
dat$Taal_SubC <- round(dat$Taal_SubC * 10, digit=2)

##_____________________________________________________________________________________________________________________
## Set reference value for regression##
##_____________________________________________________________________________________________________________________

dat$GenderV2.x <- relevel(dat$GenderV2.x, ref = "Male")
dat$KPSpre.x <- relevel(dat$KPSpre.x, ref = ">=70")
dat$lat <- relevel(dat$lat, ref = "R")
dat$saw <- relevel(dat$saw, ref = "1")

##____________________________________________________________________________________________________________________________________#
# create new dataframe with only used columns (EXCLUDED ASA, >80% missing)
##____________________________________________________________________________________________________________________________________#
dat_clean <- data.frame(
  dat$SurgeryExtend.x,
  dat$age.x,
  dat$GenderV2.x,
  dat$KPSpre.x,
  dat$group.x,
  dat$vol,
  dat$multif,
  dat$lat,
  dat$left,
  dat$right,
  dat$RI,
  dat$billat,
  dat$R_Par,
  dat$R_temp,
  dat$R_Occ,
  dat$R_Front,
  dat$L_Par,
  dat$L_Temp,
  dat$L_Occ,
  dat$L_Front,
  dat$visual,
  dat$somatomotor,
  dat$dorsalAttention,
  dat$salienceVentralAttention,
  dat$limbic,
  dat$frontoparietalControl,
  dat$default,
  dat$Thalamus,
  dat$PUT,
  dat$GP,
  dat$PrG,
  dat$Taal_Cort,
  dat$CST,
  dat$Taal_SubC,
  dat$saw,
  dat$surv,
  dat$dKPS,
  dat$death
)

#check completeness rows
nrow(dat_clean) #1134
ncol(dat_clean) # 37



##____________________________________________________________________________________________________________________________________#
#create dataframe excluding cases that have NA's
##____________________________________________________________________________________________________________________________________#
# chack number of rows with NA's
NA_rows <-dat_clean[rowSums(is.na(dat_clean[1:35])) > 0,]
nrow(NA_rows) # 81 (without outcome variables)

# create dataframe excluding rows with NA's
data <-dat_clean[rowSums(is.na(dat_clean[1:35])) == 0,]
nrow(data) #1053

#remove ".dat" in front of column names
colnames(data) <- c("SurgeryExtend.x",
                        "age.x",
                        "GenderV2.x",
                        "KPSpre.x",
                        "group.x",
                        "vol",
                        "multif",
                        "lat",
                        "left",
                        "right",
                        "RI",
                        "billat",
                        "R_Par",
                        "R_temp",
                        "R_Occ",
                        "R_Front",
                        "L_Par",
                        "L_Temp",
                        "L_Occ",
                        "L_Front",
                        "visual",
                        "somatomotor",
                        "dorsalAttention",
                        "salienceVentralAttention",
                        "limbic",
                        "frontoparietalControl",
                        "default",
                        "Thalamus",
                        "PUT",
                        "GP",
                        "PrG",
                        "Taal_Cort",
                        "CST",
                        "Taal_SubC",
                        "saw",
                        "surv",
                        "dKPS",
                        "death")

save.image(file.path(dd, file="dat_noNA_preprocessed.RData"))
citation()
# create .csv for complete cases
#write.csv(data, file.path(dd, "data_complete_noNA.csv"))







##____________________________________________________________________________________________________________________________________#
## create df with all cases including NA's
##____________________________________________________________________________________________________________________________________#

#remove ".dat" in front of colomn names
#dat_withNA <- dat_clean
#colnames(dat_withNA) <- c("SurgeryExtend.x",
                          "age.x",
                          "GenderV2.x",
                          "KPSpre.x",
                          "group.x",
                          "vol",
                          "multif",
                          "lat",
                          "left",
                          "right",
                          "RI",
                          "billat",
                          "R_Par",
                          "R_temp",
                          "R_Occ",
                          "R_Front",
                          "L_Par",
                          "L_Temp",
                          "L_Occ",
                          "L_Front",
                          "visual",
                          "somatomotor",
                          "dorsalAttention",
                          "salienceVentralAttention",
                          "limbic",
                          "frontoparietalControl",
                          "default",
                          "Thalamus",
                          "PUT",
                          "GP",
                          "PrG",
                          "Taal_Cort",
                          "CST",
                          "Taal_SubC",
                          "saw",
                          "surv",
                          "dKPS",
                          "death")

#save.image()

# create .csv for all data including NA's
# write.csv(dat_withNA, file.path(dd, "data_complete_withNA.csv"))



