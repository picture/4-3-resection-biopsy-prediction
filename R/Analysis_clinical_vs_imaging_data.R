##____________________________________________________________________________________________________________________________________#
## Data_complete load ##
##____________________________________________________________________________________________________________________________________#
dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data'
od <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R'
setwd(wd)

# load workspace.image
load(file.path(dd, 'dat_noNA_preprocessed.RData'))

#____________________________________________________________________________________________________________________________________#
# Load packages

library(usethis)
library(devtools)
library(easyGgplot2)
library(sjPlot)



##____________________________________________________________________________________________________________________________________#
## Simple Logistic regression Clinical
##____________________________________________________________________________________________________________________________________#

# set reference value
dat$GenderV2.x <- relevel(dat$GenderV2.x, ref = "Male")
dat$KPSpre.x <- relevel(dat$KPSpre.x, ref = ">=70")
dat$lat <- relevel(dat$lat, ref = "R")


#CLINICAL
logisticage <- glm(SurgeryExtend.x ~ age.x, data=dat, family="binomial")
logisticgender <- glm(SurgeryExtend.x ~ GenderV2.x, data=dat, family="binomial")
logisticKPS <- glm(SurgeryExtend.x ~ KPSpre.x, data=dat, family="binomial")
logisticgroup <- glm(SurgeryExtend.x ~ group.x, data=dat, family="binomial")

# TABLE Odds ratio's simple logistic regression
#tab_model(
  logisticage, 
  logisticgender, 
  logisticKPS, 
  show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE)


##____________________________________________________________________________________________________________________________________#
## MULTIVARIABLE LOGISTIC REGRESSION CLINICAL DATA ##
##____________________________________________________________________________________________________________________________________#

model_complete_clin <- glm(SurgeryExtend.x ~ 
                        age.x + 
                        KPSpre.x, 
                      data=dat, family="binomial")

#summary(model_complete_clin)
#tab_model(model_complete_clin)


##____________________________________________________________________________________________________________________________________#
## Simple Logistic regression Clinical
##____________________________________________________________________________________________________________________________________#

#IMAGING
logisticvol <- glm(SurgeryExtend.x ~ vol, data=dat, family="binomial")
logisticmultif <- glm(SurgeryExtend.x ~ multif, data=dat, family="binomial")
logisticlat <- glm(SurgeryExtend.x ~ lat, data=dat, family="binomial")
logisticbillat <- glm(SurgeryExtend.x ~ billat, data=dat, family="binomial")
logisticRI <- glm(SurgeryExtend.x ~ RI, data=dat, family="binomial")
logisticR_Par <- glm(SurgeryExtend.x ~ R_Par, data=dat, family="binomial")
logisticR_temp <- glm(SurgeryExtend.x ~ R_temp, data=dat, family="binomial")
logisticR_Occ <- glm(SurgeryExtend.x ~ R_Occ, data=dat, family="binomial")
logisticR_Front <- glm(SurgeryExtend.x ~ R_Front, data=dat, family="binomial")
logisticL_Par <- glm(SurgeryExtend.x ~ L_Par, data=dat, family="binomial")
logisticL_Temp <- glm(SurgeryExtend.x ~ L_Temp, data=dat, family="binomial")
logisticL_Occ <- glm(SurgeryExtend.x ~ L_Occ, data=dat, family="binomial")
logisticL_Front <- glm(SurgeryExtend.x ~ L_Front, data=dat, family="binomial")
logisticvisual <- glm(SurgeryExtend.x ~ visual, data=dat, family="binomial")
logisticsomatomotor <- glm(SurgeryExtend.x ~ somatomotor, data=dat, family="binomial")
logisticdorsalAttention <- glm(SurgeryExtend.x ~ dorsalAttention, data=dat, family="binomial")
logisticsalienceVentralAttention <- glm(SurgeryExtend.x ~ salienceVentralAttention, data=dat, family="binomial")
logisticlimbic <- glm(SurgeryExtend.x ~ limbic, data=dat, family="binomial")
logisticfrontoparietalControl <- glm(SurgeryExtend.x ~ frontoparietalControl, data=dat, family="binomial")
logisticdefault <- glm(SurgeryExtend.x ~ default, data=dat, family="binomial")
logisticCST_R <- glm(SurgeryExtend.x ~ CST_R, data=dat, family="binomial")
logisticCST_L <- glm(SurgeryExtend.x ~ CST_L, data=dat, family="binomial")
logisticArc_R <- glm(SurgeryExtend.x ~ Arc_R, data=dat, family="binomial")
logisticArc_L <- glm(SurgeryExtend.x ~ Arc_L, data=dat, family="binomial")
logisticThalamus <- glm(SurgeryExtend.x ~ Thalamus, data=dat, family="binomial")
logisticSTN <- glm(SurgeryExtend.x ~ STN, data=dat, family="binomial")
logisticbrainstem <- glm(SurgeryExtend.x ~ brainstem, data=dat, family="binomial")
logisticPUT <- glm(SurgeryExtend.x ~ PUT, data=dat, family="binomial")
logisticGP <- glm(SurgeryExtend.x ~ GP, data=dat, family="binomial")
logisticPrG <- glm(SurgeryExtend.x ~ PrG, data=dat, family="binomial")
logisticTaal_Cort <- glm(SurgeryExtend.x ~ Taal_Cort, data=dat, family="binomial")
logisticCST <- glm(SurgeryExtend.x ~ CST, data=dat, family="binomial")
logisticTaal_SubC <- glm(SurgeryExtend.x ~ Taal_SubC, data=dat, family="binomial")
logisticeloquent <- glm(SurgeryExtend.x ~ eloquent, data=dat, family="binomial")


# TABLE Odds ratio's simple logistic regression
#tab_model(
  logisticvol, 
  logisticmultif, 
  logisticlat, 
  logisticbillat, 
  logisticRI, 
  logisticR_Par, 
  logisticR_temp, 
  logisticR_Occ, 
  logisticR_Front, 
  logisticL_Par, 
  logisticL_Temp, 
  logisticL_Occ, 
  logisticL_Front, 
  logisticvisual, 
  logisticsomatomotor, 
  logisticdorsalAttention, 
  logisticsalienceVentralAttention, 
  logisticlimbic, 
  logisticfrontoparietalControl, 
  logisticdefault, 
  logisticCST_R, 
  logisticCST_L, 
  logisticArc_R, 
  logisticArc_L, 
  logisticThalamus, 
  logisticSTN, 
  logisticbrainstem, 
  logisticPUT, 
  logisticGP, 
  logisticPrG, 
  logisticTaal_Cort, 
  logisticCST, 
  logisticTaal_SubC, 
  logisticeloquent, 
  show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE
)


##____________________________________________________________________________________________________________________________________#
## MULTIVARIABLE LOGISTIC REGRESSION IMAGING DATA##
##____________________________________________________________________________________________________________________________________#

model_complete_ima <- glm(SurgeryExtend.x ~ 
                        vol + 
                        multif + 
                        lat + 
                        billat + 
                        RI +  
                        R_temp + 
                        R_Occ + 
                        R_Front +  
                        somatomotor + 
                        dorsalAttention + 
                        salienceVentralAttention + 
                        limbic + 
                        frontoparietalControl + 
                        default + 
                        Thalamus + 
                        GP + 
                        PrG + 
                        Taal_Cort + 
                        CST, 
                      data=dat, family="binomial")

#summary(model_complete_ima) #AIC 973.5
#tab_model(model_complete_ima)

# automatic glm model selection #
library(MASS)
stepAIC(model_complete_ima, trace = FALSE)

# Save output model  AIC: 965.8
model_complete_ima_AIC <- glm(formula = SurgeryExtend.x ~ vol + multif + lat + billat + 
      R_Occ + R_Front + somatomotor + dorsalAttention + salienceVentralAttention + 
      limbic + default + Thalamus + Taal_Cort + CST, family = "binomial", 
    data = dat)

#summary(model_complete_ima_AIC) #AIC: 965.8
#tab_model(model_complete_ima_AIC)

#____________________________________________________________________________________________________________________________________#
# Prediction model confusion matrix CLINICAL
# https://www.youtube.com/watch?v=ZkxyjL8SN_o
##____________________________________________________________________________________________________________________________________#

# Split into training en test dataset
set.seed(42)
train <- sample(1:nrow(dat), 0.75 * nrow(dat))
test <- dat[-train,]

# Create prediction model
SurgPred_clin <- predict(model_complete_clin, newdata = test, type = "respons")

# Round probability of to "0" or "1", > or < 0,5
p_resection_clin = round(SurgPred_clin)

# Change test data outcome from "biopsy" and "resection" to "0" and "1"
test$SurgeryExtend.x <- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

# change to factors for confusion matrix
p_resection_clin <- as.factor(p_resection_clin)
test$SurgeryExtend.x <- as.factor(test$SurgeryExtend.x)

# make confusion matrix
library(caret)
confusionMatrix(p_resection_clin, test$SurgeryExtend.x)


##____________________________________________________________________________________________________________________________________#
## ROC en AUC graphs CLINICAL ##
## https://www.youtube.com/watch?v=qcvAqAH60Yw
##____________________________________________________________________________________________________________________________________#

SurgPred_clin<- as.numeric(SurgPred_clin)

#Change test data into numeric "0" and "1"
test$SurgeryExtend.x<- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

library(pROC)

plot(SurgPred_clin, test$SurgeryExtend.x)
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred_clin, family = binomial)
lines(SurgPred_clin, glm.fit$fitted.values)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, legacy.axes=TRUE, col='#377eb8', lwd=2, print.auc=TRUE)


#____________________________________________________________________________________________________________________________________#
# Prediction model confusion matrix IMAGING
# https://www.youtube.com/watch?v=ZkxyjL8SN_o
##____________________________________________________________________________________________________________________________________#

# Split into training en test dataset
set.seed(42)
train <- sample(1:nrow(dat), 0.75 * nrow(dat))
test <- dat[-train,]

# Create prediction model
SurgPred_ima <- predict(model_complete_ima_AIC, newdata = test, type = "respons")

# Round probability of to "0" or "1", > or < 0,5
p_resection_ima = round(SurgPred_ima)

# Change test data outcome from "biopsy" and "resection" to "0" and "1"
test$SurgeryExtend.x <- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

# change to factors for confusion matrix
p_resection_ima <- as.factor(p_resection_ima)
test$SurgeryExtend.x <- as.factor(test$SurgeryExtend.x)

# make confusion matrix
confusionMatrix(p_resection_ima, test$SurgeryExtend.x)


##____________________________________________________________________________________________________________________________________#
## ROC en AUC graphs IMAGING ##
## https://www.youtube.com/watch?v=qcvAqAH60Yw
##____________________________________________________________________________________________________________________________________#

SurgPred_ima<- as.numeric(SurgPred_ima)

#Change test data into numeric "0" and "1"
test$SurgeryExtend.x<- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

glm.fit=glm(test$SurgeryExtend.x ~ SurgPred_ima, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE, legacy.axes=TRUE, col='#4daf4a', lwd=2, print.auc=TRUE, print.auc.y=0.45)
legend("bottomright", legend=c("Clinical data","Imaging data"), col=c('#377eb8',"#4daf4a"), lwd=2, text.width=0.2, bty = "n")

##____________________________________________________________________________________________________________________________________#
## MIXED (random) EFFECT MODEL (frailty terms) ##
## https://www.youtube.com/watch?v=FCcVPsq8VcA
##____________________________________________________________________________________________________________________________________#

library(lme4)
# mixed model based on model_complete_AIC with "group" as random effect
summary(MIXmodel <- glmer(formula = SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + 
                            lat + billat + RI + R_temp + R_Occ + R_Front + dorsalAttention + 
                            salienceVentralAttention + limbic + frontoparietalControl + 
                            default + Thalamus + STN + brainstem + PrG + CST + (1|group.x), 
                          family = "binomial", data = dat)
)



#____________________________________________________________________________________________________________________________________#
# Prediction model confusion matrix COMBINED
# https://www.youtube.com/watch?v=ZkxyjL8SN_o
##____________________________________________________________________________________________________________________________________#

# Split into training en test dataset
set.seed(42)
train <- sample(1:nrow(dat), 0.75 * nrow(dat))
test <- dat[-train,]

# Create prediction model
SurgPred_mix <- predict(MIXmodel, newdata = test, type = "respons")

# Round probability of to "0" or "1", > or < 0,5
p_resection_mix = round(SurgPred_mix)

# Change test data outcome from "biopsy" and "resection" to "0" and "1"
test$SurgeryExtend.x <- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

# change to factors for confusion matrix
p_resection_mix <- as.factor(p_resection_mix)
test$SurgeryExtend.x <- as.factor(test$SurgeryExtend.x)

# make confusion matrix
confusionMatrix(p_resection_mix, test$SurgeryExtend.x)


##____________________________________________________________________________________________________________________________________#
## ROC en AUC graphs COMBINED ##
## https://www.youtube.com/watch?v=qcvAqAH60Yw
##____________________________________________________________________________________________________________________________________#

SurgPred_mix<- as.numeric(SurgPred_mix)

#Change test data into numeric "0" and "1"
test$SurgeryExtend.x<- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

glm.fit=glm(test$SurgeryExtend.x ~ SurgPred_mix, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE, legacy.axes=TRUE, col='#20B2AA', lwd=4, print.auc=TRUE, print.auc.y=0.4)
legend("bottomright", legend=c("Clinical data","Imaging data", "Combined"), col=c('#377eb8',"#4daf4a", "#20B2AA"), lwd=4, text.width=0.2)

