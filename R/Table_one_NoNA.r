##____________________________________________________________________________________________________________________________________#
## load RData image
##____________________________________________________________________________________________________________________________________#

dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data'
od <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R'
setwd(wd)

# load workspace
load(file.path(dd, 'dat_noNA_preprocessed.RData'))

nrow(dat) #1053
ncol(dat) #35
#head(dat)


##____________________________________________________________________________________________________________________________________#
## Table 1 ## https://cran.r-project.org/web/packages/tableone/vignettes/introduction.html
##____________________________________________________________________________________________________________________________________#

# change back to percentage
dat$r_frontal <- as.logical(dat$R_Front)
dat$r_parietal <- as.logical(dat$R_Par)
dat$r_temporal <- as.logical(dat$R_temp)
dat$r_occipital <- as.logical(dat$R_Occ)
dat$l_frontal <- as.logical(dat$L_Front)
dat$l_parietal <- as.logical(dat$L_Par)
dat$l_temporal <- as.logical(dat$L_Temp)
dat$l_occipital <- as.logical(dat$L_Occ)

dat$prg <- as.logical(dat$PrG)
dat$taal_cort <- as.logical(dat$Taal_Cort)

dat$cst <- as.logical(dat$CST)
dat$taal_sub <- as.logical(dat$Taal_SubC)

dat$BK <- dat$Thalamus + dat$PUT + dat$GP
dat$bk <- as.logical(dat$BK) 

dat$S7vis <- as.logical(dat$visual) 
dat$S7som <- as.logical(dat$somatomotor) 
dat$S7da <- as.logical(dat$dorsalAttention) 
dat$S7va <- as.logical(dat$salienceVentralAttention) 
dat$S7lim <- as.logical(dat$limbic) 
dat$S7fpcon <- as.logical(dat$frontoparietalControl) 
dat$S7def <- as.logical(dat$default) 

dat$age.x <- dat$age.x *10
dat$vol <- dat$vol *10
dat$RI <- dat$RI *10


# change lobe involvement into logical
dat_ETZ <- dat[dat$group.x == "ETZ",]
dat_HMC <- dat[dat$group.x == "HMC",]
dat_HUM <- dat[dat$group.x == "HUM",]
dat_ISALA <- dat[dat$group.x == "ISALA",]
dat_MUW <- dat[dat$group.x == "MUW",]
dat_NWZ <- dat[dat$group.x == "NWZ",]
dat_PARIS <- dat[dat$group.x == "PARIS",]
dat_SLZ <- dat[dat$group.x == "SLZ",]
dat_UCSF <- dat[dat$group.x == "UCSF",]
dat_UMCG <- dat[dat$group.x == "UMCG",]
dat_UMCU <- dat[dat$group.x == "UMCU",]
dat_VUmc <- dat[dat$group.x == "VUmc",]

medianVars <- c("RI",
              "vol",
              "age.x")

library(tableone)

Vars <- c("age.x",
          "GenderV2.x",
          "KPSpre.x",
          "vol",
          "multif",
          "lat",
          "RI",
          "billat",
          "r_frontal",
          "r_parietal",
          "r_temporal",
          "r_occipital",
          "l_frontal",
          "l_parietal",
          "l_temporal",
          "l_occipital",
          "prg",
          "taal_cort",
          "cst",
          "taal_sub",
          "bk",
          "S7vis",
          "S7som",
          "S7da",
          "S7va",
          "S7lim",
          "S7fpcon",
          "S7def"
          )

tabETZ  <- print(CreateTableOne(vars = Vars, data = dat_ETZ, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabHMC  <- print(CreateTableOne(vars = Vars, data = dat_HMC, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabHUM  <- print(CreateTableOne(vars = Vars, data = dat_HUM, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabISALA  <- print(CreateTableOne(vars = Vars, data = dat_ISALA, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabMUW  <- print(CreateTableOne(vars = Vars, data = dat_MUW, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabNWZ  <- print(CreateTableOne(vars = Vars, data = dat_NWZ, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabPARIS  <- print(CreateTableOne(vars = Vars, data = dat_PARIS, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabSLZ  <- print(CreateTableOne(vars = Vars, data = dat_SLZ, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabUCSF  <- print(CreateTableOne(vars = Vars, data = dat_UCSF, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabUMCG  <- print(CreateTableOne(vars = Vars, data = dat_UMCG, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabUMCU  <- print(CreateTableOne(vars = Vars, data = dat_UMCU, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabVUmc  <- print(CreateTableOne(vars = Vars, data = dat_VUmc, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)
tabTOT  <- print(CreateTableOne(vars = Vars, data = dat, strata = "SurgeryExtend.x", factorVars = "GenderV2.x", test = F), nonnormal = medianVars)

TABod <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out/TableOne'

write.csv2(tabETZ, file.path(TABod, "tab1.csv"))
write.csv2(tabHMC, file.path(TABod, "tab2.csv"))
write.csv2(tabHUM, file.path(TABod, "tab3.csv"))
write.csv2(tabISALA, file.path(TABod, "tab4.csv"))
write.csv2(tabMUW, file.path(TABod, "tab5.csv"))
write.csv2(tabNWZ, file.path(TABod, "tab6.csv"))
write.csv2(tabPARIS, file.path(TABod, "tab7.csv"))
write.csv2(tabSLZ, file.path(TABod, "tab8.csv"))
write.csv2(tabUCSF , file.path(TABod, "tab9 .csv"))
write.csv2(tabUMCG , file.path(TABod, "tab10 .csv"))
write.csv2(tabUMCU , file.path(TABod, "tab11 .csv"))
write.csv2(tabVUmc , file.path(TABod, "tab12 .csv"))
write.csv2(tabTOT, file.path(TABod, "tabTOT.csv"))



##____________________________________________________________________________________________________________________________________#
## Table 1 ##
##____________________________________________________________________________________________________________________________________#



library(arsenal)

my_controls <- tableby.control(
  test = T,
  total = T,
  numeric.test = "kwt", cat.test = "chisq",
  numeric.stats = c("meansd", "Nmiss"),
  cat.stats = c("countpct", "Nmiss"),
  stats.labels = list(
    meansd = "Mean (SD)",
    Nmiss2 = "Missing"
  )
)


my_labels <- list(
  age.x = "Age",
  GenderV2.x = "Gender",
  KPSpre.x = "KPS pre-operative",
  vol = "Volume (mL)",
  multif = "Multifocal tumor",
  lat = "Laterality",
  r_frontal = "Right frontal lobe",
  r_parietal = "Right parietal lobe",
  r_temporal = "Right temporal lobe",
  r_occipital = "Right occipital lobe",
  l_frontal = "Left frontal lobe",
  l_parietal = "Left parietal lobe",
  l_temporal = "Left temporal lobe",
  l_occipital = "Left occipital lobe",
  billat = "Midline crossing tumor",
  RI = "Resection index",
  group.x = "Hospital"
)

table_one <- tableby(SurgeryExtend.x ~ 
                       age.x + 
                       GenderV2.x + 
                       KPSpre.x + 
                       vol + 
                       multif + 
                       lat + 
                       r_frontal +
                       r_parietal +
                       r_temporal +
                       r_occipital +
                       l_frontal +
                       l_parietal +
                       l_temporal +
                       l_occipital +
                       billat + 
                       RI, 
                     data = dat, control = my_controls)
summary(table_one, text = TRUE, labelTranslations = my_labels, digits=1, title = "Baseline data")


write2word(table_one, labelTranslations = my_labels, file = "table_one_word")

