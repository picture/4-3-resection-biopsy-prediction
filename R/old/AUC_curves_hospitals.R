##____________________________________________________________________________________________________________________________________#
## External validation hospitals #
##____________________________________________________________________________________________________________________________________#

unique(dat$group.x)
# ETZ HMC HUM ISALA MUW NWZ PARIS SLZ UCSF UMCG UMCU VUmc

# Select model to test with
model <- MIXmodel # of MIXmodel???

# Test per hospital
test <- dat[dat$group.x == "ETZ",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=FALSE,legacy.axes=TRUE, col='#377eb8', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "HMC",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#FFFF00', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "HUM",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#FF0000', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "ISALA",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#008000', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "MUW",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#00FF00', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "NWZ",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#800080', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "PARIS",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#FF00FF', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "SLZ",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#800000', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "UCSF",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#008080', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "UMCG",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#000080', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "UMCU",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#DFFF00', lwd=2, print.auc=FALSE)

test <- dat[dat$group.x == "VUmc",]
SurgPred <- predict(model, newdata = test, type = "respons")
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, add=TRUE,legacy.axes=TRUE, col='#00FFFF', lwd=2, print.auc=FALSE)

legend("bottomright", 
       legend=c("Hospital 1", 
                "Hospital 2",
                "Hospital 3",
                "Hospital 4",
                "Hospital 5",
                "Hospital 6",
                "Hospital 7",
                "Hospital 8",
                "Hospital 9",
                "Hospital 10",
                "Hospital 11",
                "Hospital 12"), 
       col=c('#377eb8',
             "#FFFF00",
             "#FF0000",
             "#008000",
             "#00FF00",
             "#800080",
             "#FF00FF",
             "#800000",
             "#008080",
             "#000080",
             "#DFFF00",
             "#00FFFF"), 
       lwd=2, text.width=0.2)
