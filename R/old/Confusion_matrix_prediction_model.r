
#____________________________________________________________________________________________________________________________________#
# Prediction model #
#____________________________________________________________________________________________________________________________________#

# train model on subset data (with set.seed ...)
set.seed(42)
model_train <- glm(SurgeryExtend.x ~ 
            age.x + 
            KPSpre.x + 
            vol + 
            multif + 
            lat + 
            billat + 
            RI +  
            R_temp + 
            R_Occ + 
            R_Front +  
            somatomotor + 
            dorsalAttention + 
            salienceVentralAttention + 
            limbic + 
            frontoparietalControl + 
            default + 
            Thalamus + 
            STN + 
            brainstem + 
            GP + 
            PrG + 
            Taal_Cort + 
            CST, 
          data=dat,
          family="binomial",
          subset = sample(1:nrow(dat), 0.75 * nrow(dat))
)
summary(model_train) #AIC 705.33

# AIC selection on training data model
library(MASS)
stepAIC(model_train, trace = FALSE)
# Output best model AIC 705.6
model_train_AIC <- glm(formula = SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + 
      lat + billat + RI + R_temp + R_Occ + R_Front + dorsalAttention + 
      salienceVentralAttention + limbic + frontoparietalControl + 
      default + Thalamus + STN + brainstem + PrG + CST, family = "binomial", 
    data = dat, subset = sample(1:nrow(dat), 0.75 * nrow(dat)))
tab_model(model_train_AIC)







##____________________________________________________________________________________________________________________________________#
## Prediction curve
##____________________________________________________________________________________________________________________________________#
#Change test data into numeric "0" and "1"
test$SurgeryExtend.x<- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

ggplot(test, aes(x = SurgPred, y = SurgeryExtend.x)) +
  geom_jitter(height = 0.1, size =1, alpha = 0.5) +
  geom_smooth(method = "glm",
              method.args = list(family = "binomial")) +
  theme_minimal() +
  scale_y_continuous(breaks = c(0, 1), labels = c("Biopsy", "Resection")) +
  ylab("") +
  xlab("HOMR Linear Predictor")











#____________________________________________________________________________________________________________________________________#
# Prediction model confusion matrix original
#____________________________________________________________________________________________________________________________________#

#https://www.youtube.com/watch?v=ZkxyjL8SN_o

# Split into training en test dataset
set.seed(42)
train <- sample(1:nrow(dat), 0.75 * nrow(dat))
test <- dat[-train,]

# Create prediction model
SurgPred_mix <- predict(MIXmodel, newdata = test, type = "respons")

# Round probability of to "0" or "1", > or < 0,5
p_resection_mix = round(SurgPred_mix)

# Change test data outcome from "biopsy" and "resection" to "0" and "1"
unique(test$SurgeryExtend.x)
class(test$SurgeryExtend.x)
test$SurgeryExtend.x <- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

# change to factors for confusion matrix
p_resection_mix <- as.factor(p_resection_mix)
test$SurgeryExtend.x <- as.factor(test$SurgeryExtend.x)

# make confusion matrix
library(caret)
confusionMatrix(p_resection_mix, test$SurgeryExtend.x)

#predictionmodel_results <- confusionMatrix(p_resection, test$SurgeryExtend.x) 
#capture.output(confusionMatrix(p_resection, test$SurgeryExtend.x), file.path(od, "predictionmodel_results.csv"))

#as.table(predictionmodel_results)
#results1 <- as.matrix(predictionmodel_results, what = "overall")
#results2 <- as.matrix(predictionmodel_results, what = "classes")
#write.csv(results1, file.path(od, "results_1_confusion matrix.csv"))
#write.csv(results2, file.path(od, "results_2_confusion matrix.csv"))



##____________________________________________________________________________________________________________________________________#
## ROC en AUC graphs original ##
##____________________________________________________________________________________________________________________________________#
# https://www.youtube.com/watch?v=qcvAqAH60Yw

SurgPred_mix<- as.numeric(SurgPred_mix)

#Change test data into numeric "0" and "1"
test$SurgeryExtend.x<- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

library(pROC)
library(randomForest)

plot(SurgPred_mix, test$SurgeryExtend.x)
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred_mix, family = binomial)
lines(SurgPred_mix, glm.fit$fitted.values)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, legacy.axes=TRUE, col='#377eb8', lwd=4, print.auc=TRUE)

# Add Random Forest model
rf.model <- randomForest(factor(test$SurgeryExtend.x) ~ SurgPred_mix)
plot.roc(test$SurgeryExtend.x, rf.model$votes[,1], legacy.axes=TRUE, col="#4daf4a", print.auc=TRUE, add=TRUE, print.auc.y=0.4)
legend("bottomright", legend=c("Logistic Regression", "Random Forest"), col=c('#377eb8',"#4daf4a" ), lwd=4, text.width=0.5)

#____________________________________________________________________________________________________________________________________#
# Interaction terms  https://www.youtube.com/watch?v=oVeMquBjNYs
summary(INTmodel1 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + ENTvolML + Multifocal + age.x:KPSpre.x + age.x:ENTside + age.x:ENTvolML + age.x:Multifocal + KPSpre.x:ENTside + KPSpre.x:ENTvolML + KPSpre.x:Multifocal + ENTside:ENTvolML + ENTside:Multifocal + ENTvolML:Multifocal, data=dat, family="binomial"))
tab_model(INTmodel1)



