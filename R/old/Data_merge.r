#____________________________________________________________________________________________________________________________________#
# Merge data XNAT with trondheim image data to data_complete.csv
#____________________________________________________________________________________________________________________________________#


dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data/updateMirror'
dd_trond <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data/Trondheim'
od <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R'
setwd(wd)

# import data
data <- read.csv(file.path(dd, 'all_subjects.csv'))
head(data)

# import segmentation & white matter tract data Trondheim
data_seg <- read.csv(file.path(dd_trond, 'complete_diagnosis_automatic_segmentations_xnat.csv'))
head(data_seg)
data_wm <- read.csv(file.path(dd_trond, 'white_matter_features_overall.csv'))
head(data_wm)


# New column same name as "data" column
data_seg$ID <- data_seg$XNAT_ID

data_comb <- merge(data,data_seg,by="ID")
write.csv(data_comb, file.path(od, "data_comb.csv"))

data_complete <- merge(data_comb, data_wm, by= "DBID")

# Exclude NA in biosy or resection and create new DF
data_complete$SurgeryExtend.x [data_complete$SurgeryExtend.x == ""] <- NA #rename missing to NA
data_complete <- data_complete[!is.na(data_complete$SurgeryExtend.x),]
head(data_complete)

# Total cases with available outcome data
data_complete <- subset(data_complete, Category == "Automatic")
nrow(data_complete) #1134 (1318 voor merge)
ncol(data_complete) #458
head(data_complete)


write.csv(data_complete, file.path(od, "data_complete.csv"))