
##____________________________________________________________________________________________________________________________________#
## MIXED (random) EFFECT MODEL (frailty terms) ##
##____________________________________________________________________________________________________________________________________#
                                                # https://www.youtube.com/watch?v=FCcVPsq8VcA
                                                # https://m-clark.github.io/mixed-models-with-R/random_intercepts.html
                                                # https://ourcodingclub.github.io/tutorials/mixed-models/
library(lme4)
# mixed model based on model_complete_AIC with "group" as random effect
# REMOVED STN DUE TO LARGE OR AND FEW OBSERVATIONS
summary(MIXmodel <- glmer(formula = SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + 
                            lat + billat + RI + R_Occ + R_Front + dorsalAttention + 
                            salienceVentralAttention + limbic + default + PrG + Taal_Cort + CST
                            + (1|group.x), 
                      family = "binomial", data = dat)
)



print(MIXmodel, correlation=TRUE)
tab_model(MIXmodel)  #§ interpretation? ICC = 0.11, dus 0.11 van de variance wordt verklaard door groepverschillen? http://cran.nexr.com/web/packages/sjstats/vignettes/mixedmodels-statistics.html

#plot(MIXmodel)

#confint(MIXmodel)
#ranef(MIXmodel)$group.x
#coef(MIXmodel)$group.x

#Plot
#library(merTools)
#predictInterval(MIXmodel)
#REsim(MIXmodel)
#plotREsim(REsim(MIXmodel))

#anova(model1, MIXmodel1) #compare models










