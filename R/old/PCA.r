##____________________________________________________________________________________________________________________________________#
## Data_complete load ##
##____________________________________________________________________________________________________________________________________#

dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data'
od <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R'
setwd(wd)

# import data
dat <- read.csv(file.path(dd, 'data_complete_noNA.csv'))
nrow(dat) #1053
#head(dat)
#____________________________________________________________________________________________________________________________________#
# Load packages

library(usethis)
library(devtools)
library(easyGgplot2)
library(sjPlot)

##____________________________________________________________________________________________________________________________________#
## Check & restructure data ##
##____________________________________________________________________________________________________________________________________#

# change data class
dat$SurgeryExtend.x <- as.factor(dat$SurgeryExtend.x)
dat$GenderV2.x <- as.factor(dat$GenderV2.x)
dat$KPSpre.x <- as.factor(dat$KPSpre.x)
dat$group.x <- as.factor(dat$group.x)
dat$vol <- as.numeric(dat$vol)
dat$multif <- as.logical(dat$multif)
dat$billat <- as.logical(dat$billat)
#class(dat$age.x) #numeric
#class(dat$left) #numeric
# class(dat$RI) #numeric

##_____________________________________________________________________________________________________________________
## Rescore data ##
##_____________________________________________________________________________________________________________________
library(dplyr)

# binarize laterality
dat$lat <- dat$left
dat$lat [dat$lat < 50] <- "R"
dat$lat [dat$lat >50 & dat$lat != "R"] <- "L"
dat$lat [dat$lat == 100] <- "L"
dat$lat <- as.factor(dat$lat)

# change RI to %
dat$RI <- dat$RI * 100

# change CST to %
dat$CST <- round(dat$CST*100, digit=2)


##____________________________________________________________________________________________________________________________________#
## Simple Logistic regression
##____________________________________________________________________________________________________________________________________#

# set reference value
dat$GenderV2.x <- relevel(dat$GenderV2.x, ref = "Male")
dat$KPSpre.x <- relevel(dat$KPSpre.x, ref = ">=70")
dat$lat <- relevel(dat$lat, ref = "R")

# rescore RI to percentage
#library(scales)
#dat$RI_p <- percent(dat$RI)


##____________________________________________________________________________________________________________________________________#
## Principal component analysis (only possible for numeric variables)
##____________________________________________________________________________________________________________________________________#

#Dataframe with included NUMERIC variables

dat_pca <- dat[,c( # data selected from significant variables form multif glm
  "age.x",
  "vol",
  "RI",
  "R_Occ",
  "dorsalAttention",
  "salienceVentralAttention",
  "limbic",
  "default",
  "PrG",
  "CST")]

pca <- prcomp(dat_pca, scale. = TRUE)
?prcomp()
## plot pc1 and pc2
plot(pca$x[,1], pca$x[,2])

## make a scree plot
pca.var <- pca$sdev^2
pca.var.per <- round(pca.var/sum(pca.var)*100, 1)

barplot(pca.var.per, main="Scree Plot", xlab="Principal Component", ylab="Percent Variation")


## now make a fancy looking plot that shows the PCs and the variation:
library(ggplot2)

pca.data <- data.frame(Sample=rownames(pca$x),
                       X=pca$x[,1],
                       Y=pca$x[,2])
pca.data

ggplot(data=pca.data, aes(x=X, y=Y, label=Sample)) +
  geom_text() +
  xlab(paste("PC1 - ", pca.var.per[1], "%", sep="")) +
  ylab(paste("PC2 - ", pca.var.per[2], "%", sep="")) +
  theme_bw() +
  ggtitle("My PCA Graph")

## get the name of the top 10 measurements (genes) that contribute
## most to pc1.
loading_scores <- pca$rotation[,1]
gene_scores <- abs(loading_scores) ## get the magnitudes
gene_score_ranked <- sort(gene_scores, decreasing=TRUE)
top_10_genes <- names(gene_score_ranked[1:10])

top_10_genes ## show the names of the top 10 genes

pca$rotation[top_10_genes,1] ## show the scores (and +/- sign)

#######
##
## NOTE: Everything that follow is just bonus stuff.
## It simply demonstrates how to get the same
## results using "svd()" (Singular Value Decomposition) or using "eigen()"
## (Eigen Decomposition).
##
#######

############################################
##
## Now let's do the same thing with svd()
##
## svd() returns three things
## v = the "rotation" that prcomp() returns, this is a matrix of eigenvectors
##     in other words, a matrix of loading scores
## u = this is similar to the "x" that prcomp() returns. In other words,
##     sum(the rotation * the original data), but compressed to the unit vector
##     You can spread it out by multiplying by "d"
## d = this is similar to the "sdev" value that prcomp() returns (and thus
##     related to the eigen values), but not
##     scaled by sample size in an unbiased way (ie. 1/(n-1)).
##     For prcomp(), sdev = sqrt(var) = sqrt(ss(fit)/(n-1))
##     For svd(), d = sqrt(ss(fit))
##
############################################

svd.stuff <- svd(scale(t(dat_pca), center=TRUE))

## calculate the PCs
svd.data <- data.frame(Sample=colnames(dat_pca),
                       X=(svd.stuff$u[,1] * svd.stuff$d[1]),
                       Y=(svd.stuff$u[,2] * svd.stuff$d[2]))
svd.data

## alternatively, we could compute the PCs with the eigen vectors and the
## original data
svd.pcs <- t(t(svd.stuff$v) %*% t(scale(t(dat_pca), center=TRUE)))
svd.pcs[,1:2] ## the first to principal components

svd.df <- ncol(dat_pca) - 1
svd.var <- svd.stuff$d^2 / svd.df
svd.var.per <- round(svd.var/sum(svd.var)*100, 1)

ggplot(data=svd.data, aes(x=X, y=Y, label=Sample)) +
  geom_text() +
  xlab(paste("PC1 - ", svd.var.per[1], "%", sep="")) +
  ylab(paste("PC2 - ", svd.var.per[2], "%", sep="")) +
  theme_bw() +
  ggtitle("svd(scale(t(data.matrix), center=TRUE)")

############################################
##
## Now let's do the same thing with eigen()
##
## eigen() returns two things...
## vectors = eigen vectors (vectors of loading scores)
##           NOTE: pcs = sum(loading scores * values for sample)
## values = eigen values
##
############################################
cov.mat <- cov(scale(t(dat_pca), center=TRUE))
dim(cov.mat)

## since the covariance matrix is symmetric, we can tell eigen() to just
## work on the lower triangle with "symmetric=TRUE"
eigen.stuff <- eigen(cov.mat, symmetric=TRUE)
dim(eigen.stuff$vectors)
head(eigen.stuff$vectors[,1:2])

eigen.pcs <- t(t(eigen.stuff$vectors) %*% t(scale(t(dat_pca), center=TRUE)))
eigen.pcs[,1:2]

eigen.data <- data.frame(Sample=rownames(eigen.pcs),
                         X=(-1 * eigen.pcs[,1]), ## eigen() flips the X-axis in this case, so we flip it back
                         Y=eigen.pcs[,2]) ## X axis will be PC1, Y axis will be PC2
eigen.data

eigen.var.per <- round(eigen.stuff$values/sum(eigen.stuff$values)*100, 1)

ggplot(data=eigen.data, aes(x=X, y=Y, label=Sample)) +
  geom_text() +
  xlab(paste("PC1 - ", eigen.var.per[1], "%", sep="")) +
  ylab(paste("PC2 - ", eigen.var.per[2], "%", sep="")) +
  theme_bw() +
  ggtitle("eigen on cov(t(data.matrix))")
