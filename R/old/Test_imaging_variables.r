## Test variables ##

library(usethis)
library(devtools)
library(easyGgplot2)
library(sjPlot)
#________________________________________________________________________________________________________

# Right parietal overlap
dat$R_Par <- dat$MNI_parietal_right_Main

# R_Par
dat$R_Par [dat$R_Par == "-1"] <- NA
#dat$R_Par [dat$R_Par < "5"] <- NA

# Plot Right Parietal 
# Build dataset with variables
dat_R_Par <- data.frame(dat$SurgeryExtend.x, dat$R_Par)
head(dat_R_Par)

# Multiple histograms on the same plot
R_Par_plot <- ggplot2.histogram(data=dat_R_Par, xName='dat.R_Par',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(R_Par_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticR_Par <- glm(SurgeryExtend.x ~ R_Par, data=dat, family="binomial"))
tab_model(logisticR_Par)


#________________________________________________________________________________________________________
# Right temporal overlap
dat$R_temp <- dat$MNI_temporal_right_Main

# R_temp
dat$R_temp [dat$R_temp == "-1"] <- NA

# Plot Right temporal 
# Build dataset with variables
dat_R_temp <- data.frame(dat$SurgeryExtend.x, dat$R_temp)
head(dat_R_temp)

# Multiple histograms on the same plot
R_temp_plot <- ggplot2.histogram(data=dat_R_temp, xName='dat.R_temp',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(R_temp_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticR_temp <- glm(SurgeryExtend.x ~ R_temp, data=dat, family="binomial"))
tab_model(logisticR_temp)


#________________________________________________________________________________________________________

# Right Occipital overlap
dat$R_Occ <- dat$MNI_occipital_right_Main

# R_Occ
dat$R_Occ [dat$R_Occ == "-1"] <- NA

# Plot Right Occipital
# Build dataset with variables
dat_R_Occ <- data.frame(dat$SurgeryExtend.x, dat$R_Occ)
head(dat_R_Occ)

# Multiple histograms on the same plot
R_Occ_plot <- ggplot2.histogram(data=dat_R_Occ, xName='dat.R_Occ',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(R_Occ_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticR_Occ <- glm(SurgeryExtend.x ~ R_Occ, data=dat, family="binomial"))
tab_model(logisticR_Occ)


#________________________________________________________________________________________________________

# Right frontal overlap
dat$R_Front <- dat$MNI_frontal_right_Main

# R_Front
dat$R_Front [dat$R_Front == "-1"] <- NA
#dat$R_Front [dat$R_Front < "5"] <- NA

# Plot Right frontal
# Build dataset with variables
dat_R_Front <- data.frame(dat$SurgeryExtend.x, dat$R_Front)
head(dat_R_Front)

# Multiple histograms on the same plot
R_Front_plot <- ggplot2.histogram(data=dat_R_Front, xName='dat.R_Front',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(R_Front_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticR_Front <- glm(SurgeryExtend.x ~ R_Front, data=dat, family="binomial"))
tab_model(logisticR_Front)


#________________________________________________________________________________________________________


# Left Parietal overlap
dat$L_Par <- dat$MNI_parietal_left_Main

# L_Par
dat$L_Par [dat$L_Par == "-1"] <- NA

# Plot Left parietal
# Build dataset with variables
dat_L_Par <- data.frame(dat$SurgeryExtend.x, dat$L_Par)
head(dat_L_Par)

# Multiple histograms on the same plot
L_Par_plot <- ggplot2.histogram(data=dat_L_Par, xName='dat.L_Par',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(L_Par_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticL_Par <- glm(SurgeryExtend.x ~ L_Par, data=dat, family="binomial"))
tab_model(logisticL_Par) # ! significant


#________________________________________________________________________________________________________

# Left temporal overlap
dat$L_Temp <- dat$MNI_temporal_left_Main

# L_Temp
dat$L_Temp [dat$L_Temp == "-1"] <- NA
#dat$L_Temp [dat$L_Temp < "5"] <- NA

# Plot Left temporal
# Build dataset with variables
dat_L_Temp <- data.frame(dat$SurgeryExtend.x, dat$L_Temp)
head(dat_L_Temp)

# Multiple histograms on the same plot
L_Temp_plot <- ggplot2.histogram(data=dat_L_Temp, xName='dat.L_Temp',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(L_Temp_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticL_Temp <- glm(SurgeryExtend.x ~ L_Temp, data=dat, family="binomial"))
tab_model(logisticL_Temp) # ! significant


#________________________________________________________________________________________________________

# Left occipital overlap
dat$L_Occ <- dat$MNI_occipital_left_Main

# L_Occ
dat$L_Occ [dat$L_Occ == "-1"] <- NA

# Plot Left occipital
# Build dataset with variables
dat_L_Occ <- data.frame(dat$SurgeryExtend.x, dat$L_Occ)
head(dat_L_Occ)

# Multiple histograms on the same plot
L_Occ_plot <- ggplot2.histogram(data=dat_L_Occ, xName='dat.L_Occ',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(L_Occ_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticL_Occ <- glm(SurgeryExtend.x ~ L_Occ, data=dat, family="binomial"))
tab_model(logisticL_Occ) # ! significant


#________________________________________________________________________________________________________


# Left frontal overlap
dat$L_Front <- dat$MNI_frontal_left_Main

# L_Front
dat$L_Front [dat$L_Front == "-1"] <- NA

# Plot Left frontal
# Build dataset with variables
dat_L_Front <- data.frame(dat$SurgeryExtend.x, dat$L_Front)
head(dat_L_Front)

# Multiple histograms on the same plot
L_Front_plot <- ggplot2.histogram(data=dat_L_Front, xName='dat.L_Front',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(L_Front_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticL_Front <- glm(SurgeryExtend.x ~ L_Front, data=dat, family="binomial"))
tab_model(logisticL_Front)


#________________________________________________________________________________________________________


# Scheafer visual overlap
dat$visual <- dat$Schaefer7_visual_Main

# visual
dat$visual [dat$visual == "-1"] <- NA
#dat$visual [dat$visual < "2"] <- NA

# Plot Visual
# Build dataset with variables
dat_visual <- data.frame(dat$SurgeryExtend.x, dat$visual)
head(dat_visual)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_visual, xName='dat.visual',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticvisual <- glm(SurgeryExtend.x ~ visual, data=dat, family="binomial"))
tab_model(logisticvisual)


#________________________________________________________________________________________________________

# Scheafer somatomotor overlap
dat$somatomotor <- dat$Schaefer7_somatomotor_Main

# somatomotor
dat$somatomotor [dat$somatomotor == "-1"] <- NA

# Plot somatomotor
# Build dataset with variables
dat_somatomotor <- data.frame(dat$SurgeryExtend.x, dat$somatomotor)
head(dat_somatomotor)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_somatomotor, xName='dat.somatomotor',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticsomatomotor <- glm(SurgeryExtend.x ~ somatomotor, data=dat, family="binomial"))
tab_model(logisticsomatomotor)  #significant


#________________________________________________________________________________________________________

# Scheafer dorsalAttention overlap
dat$dorsalAttention <- dat$Schaefer7_dorsalAttention_Main

# dorsalAttention
dat$dorsalAttention [dat$dorsalAttention == "-1"] <- NA

# Plot dorsalAttention
# Build dataset with variables
dat_dorsalAttention <- data.frame(dat$SurgeryExtend.x, dat$dorsalAttention)
head(dat_dorsalAttention)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_dorsalAttention, xName='dat.dorsalAttention',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticdorsalAttention <- glm(SurgeryExtend.x ~ dorsalAttention, data=dat, family="binomial"))
tab_model(logisticdorsalAttention) #significant


#________________________________________________________________________________________________________

# Scheafer salienceVentralAttention overlap
dat$salienceVentralAttention <- dat$Schaefer7_salienceVentralAttention_Main

# salienceVentralAttention
dat$salienceVentralAttention [dat$salienceVentralAttention == "-1"] <- NA

# Plot salienceVentralAttention
# Build dataset with variables
dat_salienceVentralAttention <- data.frame(dat$SurgeryExtend.x, dat$salienceVentralAttention)
head(dat_salienceVentralAttention)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_salienceVentralAttention, xName='dat.salienceVentralAttention',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticsalienceVentralAttention <- glm(SurgeryExtend.x ~ salienceVentralAttention, data=dat, family="binomial"))
tab_model(logisticsalienceVentralAttention) #significant


#________________________________________________________________________________________________________

# Scheafer limbic overlap
dat$limbic <- dat$Schaefer7_limbic_Main

# limbic
dat$limbic [dat$limbic == "-1"] <- NA

# Plot limbic
# Build dataset with variables
dat_limbic <- data.frame(dat$SurgeryExtend.x, dat$limbic)
head(dat_limbic)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_limbic, xName='dat.limbic',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticlimbic <- glm(SurgeryExtend.x ~ limbic, data=dat, family="binomial"))
tab_model(logisticlimbic) #significant


#________________________________________________________________________________________________________

# Scheafer frontoparietalControl overlap
dat$frontoparietalControl <- dat$Schaefer7_frontoparietalControl_Main

# frontoparietalControl
dat$frontoparietalControl [dat$frontoparietalControl == "-1"] <- NA

# Plot frontoparietalControl
# Build dataset with variables
dat_frontoparietalControl <- data.frame(dat$SurgeryExtend.x, dat$frontoparietalControl)
head(dat_frontoparietalControl)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_frontoparietalControl, xName='dat.frontoparietalControl',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticfrontoparietalControl <- glm(SurgeryExtend.x ~ frontoparietalControl, data=dat, family="binomial"))
tab_model(logisticfrontoparietalControl) #significant


#________________________________________________________________________________________________________

# Scheafer default overlap
dat$default <- dat$Schaefer7_default_Main

# default
dat$default [dat$default == "-1"] <- NA

# Plot default
# Build dataset with variables
dat_default <- data.frame(dat$SurgeryExtend.x, dat$default)
head(dat_default)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_default, xName='dat.default',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticdefault <- glm(SurgeryExtend.x ~ default, data=dat, family="binomial"))
tab_model(logisticdefault) #significant


#________________________________________________________________________________________________________

# Scheafer somatomotorA overlap
dat$somatomotorA <- dat$Schaefer17_somatomotorA_Main

# somatomotorA
dat$somatomotorA [dat$somatomotorA == "-1"] <- NA

# Plot somatomotorA
# Build dataset with variables
dat_somatomotorA <- data.frame(dat$SurgeryExtend.x, dat$somatomotorA)
head(dat_somatomotorA)

# Multiple histograms on the same plot
somatomotor_plot <- ggplot2.histogram(data=dat_somatomotorA, xName='dat.somatomotorA',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(somatomotor_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticsomatomotorA <- glm(SurgeryExtend.x ~ somatomotorA, data=dat, family="binomial"))
tab_model(logisticsomatomotorA) #significant


#________________________________________________________________________________________________________

# Cortico Spinal Right Overlap
dat$CST_R <- dat$overlap_Cortico_Spinal_Right_Main

# CST_R
dat$CST_R [dat$CST_R == "-1"] <- NA
#dat$CST_R [dat$CST_R <"0.05"] <- NA

# Plot CST_R
# Build dataset with variables
dat_CST_R <- data.frame(dat$SurgeryExtend.x, dat$CST_R)
head(dat_CST_R)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_CST_R, xName='dat.CST_R',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticCST_R <- glm(SurgeryExtend.x ~ CST_R, data=dat, family="binomial"))
tab_model(logisticCST_R) # Bij meer dan 5% overlap wel significant


#________________________________________________________________________________________________________

# Cortico Spinal Left Overlap
dat$CST_L <- dat$overlap_Cortico_Spinal_Left_Main

# CST_L
dat$CST_L [dat$CST_L == "-1"] <- NA
dat$CST_L [dat$CST_L < "0.05"] <- NA

# Plot CST_L
# Build dataset with variables
dat_CST_L <- data.frame(dat$SurgeryExtend.x, dat$CST_L)
head(dat_CST_L)

# Multiple histograms on the same plot
CST_L_plot <- ggplot2.histogram(data=dat_CST_L, xName='dat.CST_L',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(CST_L_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticCST_L <- glm(SurgeryExtend.x ~ CST_L, data=dat, family="binomial"))
tab_model(logisticCST_L) # wel significant


#________________________________________________________________________________________________________

# Arcuate Right Overlap
dat$Arc_R <- dat$overlap_Arcuate_Right_Main

# Arc_R
dat$Arc_R [dat$Arc_R == "-1"] <- NA
#dat$Arc_R [dat$Arc_R < "0.05"] <- NA

# Plot Arc_R
# Build dataset with variables
dat_Arc_R <- data.frame(dat$SurgeryExtend.x, dat$Arc_R)
head(dat_Arc_R)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_Arc_R, xName='dat.Arc_R',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticArc_R <- glm(SurgeryExtend.x ~ Arc_R, data=dat, family="binomial"))
tab_model(logisticArc_R) # ! significant


#________________________________________________________________________________________________________

# Arcuate Left Overlap
dat$Arc_L <- dat$overlap_Arcuate_Left_Main

# Arc_L
dat$Arc_L [dat$Arc_L == "-1"] <- NA

# Plot Arc_L
# Build dataset with variables
dat_Arc_L <- data.frame(dat$SurgeryExtend.x, dat$Arc_L)
head(dat_Arc_L)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_Arc_L, xName='dat.Arc_L',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticArc_L <- glm(SurgeryExtend.x ~ Arc_L, data=dat, family="binomial"))
tab_model(logisticArc_L) # ! significant



#____________________________________________________________________________________________________________________________________#
# TABLE UNIVARIABLE LOGISTIC REGRESSION
#____________________________________________________________________________________________________________________________________#
tab_model(
    logisticR_Par,
    logisticR_temp,
    logisticR_Occ,
    logisticR_Front,
    logisticL_Par,
    logisticL_Temp,
    logisticL_Occ,
    logisticL_Front,
    logisticvisual,
    logisticsomatomotor,
    logisticdorsalAttention,
    logisticsalienceVentralAttention,
    logisticlimbic,
    logisticfrontoparietalControl,
    logisticdefault,
    logisticsomatomotorA,
    logisticCST_R,
    logisticCST_L,
    logisticArc_R,
    logisticArc_L,
    show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE
)
## are we measuring effect of size (bigger tumors have more chance of resection)
## add size as interaction term?

summary(INTmodel2 <- glm(SurgeryExtend.x ~ somatomotor + vol + somatomotor:vol, data=dat, family="binomial"))
tab_model(INTmodel2)




#____________________________________________________________________________________________________________________________________#
# ELOQUENCE 
#____________________________________________________________________________________________________________________________________#


# Thalamus Overlap
dat$Thalamus <- dat$MNI_thalamus_left_Main + dat$MNI_thalamus_right_Main

# Plot Thalamus
# Build dataset with variables
dat_Thalamus <- data.frame(dat$SurgeryExtend.x, dat$Thalamus)
head(dat_Thalamus)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_Thalamus, xName='dat.Thalamus',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticThalamus <- glm(SurgeryExtend.x ~ Thalamus, data=dat, family="binomial"))
tab_model(logisticThalamus) 


#____________________________________________________________________________________________________________________________________#

# STN Overlap
dat$STN <- dat$MNI_subthalamic_nucleus_left_Main + dat$MNI_subthalamic_nucleus_right_Main

# Deal with unknowns
dat$STN [dat$STN == "-2"] <- NA

# Plot STN
# Build dataset with variables
dat_STN <- data.frame(dat$SurgeryExtend.x, dat$STN)
head(dat_STN)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_STN, xName='dat.STN',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticSTN <- glm(SurgeryExtend.x ~ STN, data=dat, family="binomial"))
tab_model(logisticSTN) 


#____________________________________________________________________________________________________________________________________#

# brainstem Overlap
dat$brainstem <- dat$MNI_brain_stem_None_Main

# Deal with unknowns
dat$brainstem [dat$brainstem == "-1"] <- NA

# Plot brainstem
# Build dataset with variables
dat_brainstem <- data.frame(dat$SurgeryExtend.x, dat$brainstem)
head(dat_brainstem)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_brainstem, xName='dat.brainstem',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticbrainstem <- glm(SurgeryExtend.x ~ brainstem, data=dat, family="binomial"))
tab_model(logisticbrainstem) 


#____________________________________________________________________________________________________________________________________#

# PUT Overlap
dat$PUT <- dat$MNI_putamen_left_Main + dat$MNI_putamen_right_Main

# Deal with unknowns
dat$PUT [dat$PUT == "-2"] <- NA

# Plot PUT
# Build dataset with variables
dat_PUT <- data.frame(dat$SurgeryExtend.x, dat$PUT)
head(dat_PUT)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_PUT, xName='dat.PUT',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticPUT <- glm(SurgeryExtend.x ~ PUT, data=dat, family="binomial"))
tab_model(logisticPUT) 


#____________________________________________________________________________________________________________________________________#

# GP Overlap
dat$GP <- dat$MNI_globus_pallidus._left_Main + dat$MNI_globus_pallidus._right_Main

# Deal with unknowns
dat$GP [dat$GP == "-2"] <- NA

# Plot GP
# Build dataset with variables
dat_GP <- data.frame(dat$SurgeryExtend.x, dat$GP)
head(dat_GP)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_GP, xName='dat.GP',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticGP <- glm(SurgeryExtend.x ~ GP, data=dat, family="binomial"))
tab_model(logisticGP) 


#____________________________________________________________________________________________________________________________________#

# PrG Overlap
dat$PrG <- dat$Harvard.Oxford_PrG_Main

# Deal with unknowns
dat$PrG [dat$PrG == "-1"] <- NA

# Plot PrG
# Build dataset with variables
dat_PrG <- data.frame(dat$SurgeryExtend.x, dat$PrG)
head(dat_PrG)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_PrG, xName='dat.PrG',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticPrG <- glm(SurgeryExtend.x ~ PrG, data=dat, family="binomial"))
tab_model(logisticPrG) 


#____________________________________________________________________________________________________________________________________#

# Taal_Cort Overlap 
dat$Taal_Cort <- dat$Harvard.Oxford_SmGa_Main + dat$Harvard.Oxford_SmGp_Main + dat$Harvard.Oxford_AG_Main + dat$Harvard.Oxford_STGp_Main

# ALLEEN LINKS
dat$Taal_Cort [dat$lat == "R"] <- 0

# Deal with unknowns
dat$Taal_Cort [dat$Taal_Cort == "-4"] <- NA

# Plot Taal_Cort
# Build dataset with variables
dat_Taal_Cort <- data.frame(dat$SurgeryExtend.x, dat$Taal_Cort)
head(dat_Taal_Cort)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_Taal_Cort, xName='dat.Taal_Cort',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticTaal_Cort <- glm(SurgeryExtend.x ~ Taal_Cort, data=dat, family="binomial"))
tab_model(logisticTaal_Cort) 


#____________________________________________________________________________________________________________________________________#

# CST Overlap 
dat$CST <- dat$overlap_Cortico_Spinal_Left_Main + dat$overlap_Cortico_Spinal_Right_Main

# Deal with unknowns
dat$CST [dat$CST == "-2"] <- NA

# Plot CST
# Build dataset with variables
dat_CST <- data.frame(dat$SurgeryExtend.x, dat$CST)
head(dat_CST)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_CST, xName='dat.CST',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticCST <- glm(SurgeryExtend.x ~ CST, data=dat, family="binomial"))
tab_model(logisticCST) 


#____________________________________________________________________________________________________________________________________#

# Taal_SubC Overlap 
dat$Taal_SubC <- dat$overlap_Anterior_Segment_Left_Main + dat$overlap_Posterior_Segment_Left_Main + dat$overlap_Long_Segment_Left_Main + dat$overlap_Arcuate_Left_Main

# Deal with unknowns
dat$Taal_SubC [dat$Taal_SubC == "-4"] <- NA

# Plot Taal_SubC
# Build dataset with variables
dat_Taal_SubC <- data.frame(dat$SurgeryExtend.x, dat$Taal_SubC)
head(dat_Taal_SubC)

# Multiple histograms on the same plot
visual_plot <- ggplot2.histogram(data=dat_Taal_SubC, xName='dat.Taal_SubC',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
ggplot2.customize(visual_plot,
                 legendPosition="top",
                 xtitle="Overlap (%)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# Logistic regression
summary(logisticTaal_SubC <- glm(SurgeryExtend.x ~ Taal_SubC, data=dat, family="binomial"))
tab_model(logisticTaal_SubC) 
#__________________________________________________________________________________________
__________________________________________#

tab_model(logisticThalamus,logisticSTN,logisticbrainstem,logisticPUT,logisticGP,logisticPrG,logisticTaal_Cort,logisticCST,logisticTaal_SubC,show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE
)

# Multivariable model

summary(model_IM <- glm(SurgeryExtend.x ~ Thalamus +
        STN +
        brainstem +
        
        GP +
        PrG +
        Taal_Cort +
        CST +
        Taal_SubC +
        R_temp +
        R_Occ +
        R_Front +
        somatomotor +
        dorsalAttention +
        salienceVentralAttention +
        limbic +
        frontoparietalControl +
        default,   
        data=dat, family="binomial"))

tab_model(model_IM) 

# automatic glm model selection #

stepAIC(model_IM)

stepAIC(model_IM,
      scope = list(upper = ~ Thalamus +
        STN +
        brainstem +
        GP +
        PrG +
        Taal_Cort +
        CST +
        Taal_SubC +
        R_temp +
        R_Occ +
        R_Front +
        somatomotor +
        dorsalAttention +
        salienceVentralAttention +
        limbic +
        frontoparietalControl +
        default,  
        lower = ~1),
      trace = FALSE)

# Run best model
 Model_IM_best <- glm(formula = SurgeryExtend.x ~ Thalamus + STN + brainstem + 
    R_temp + R_Occ + R_Front + somatomotor + dorsalAttention + 
    salienceVentralAttention + limbic + frontoparietalControl + 
    default, family = "binomial", data = dat)

tab_model(Model_IM_best)

SurgPred_IM <- predict(Model_IM_best,newdata = test, type = "respons")
p_resection_IM = round(SurgPred_IM)

# Change test data outcome from "biopsy" and "resection" to "0" and "1"
unique(test$SurgeryExtend.x)
class(test$SurgeryExtend.x)
test$SurgeryExtend.x <- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

# change to factors for confusion matrix
p_resection_IM <- as.factor(p_resection_IM)
test$SurgeryExtend.x <- as.factor(test$SurgeryExtend.x)

# make confusion matrix
library(caret)
confusionMatrix(p_resection_IM, test$SurgeryExtend.x)

#            Reference
#Prediction   0   1
#         0  20  11
#         1  41 153

#____________________________________________________________________________________________________________________________________#
# Eloqouence measure based on Sawaya : No relation in plot, wel significant

dat$eloquent = dat$visual + dat$somatomotor + dat$Thalamus + dat$brainstem + dat$PUT + dat$GP + (dat$CST*100) + dat$Taal_Cort + (dat$Taal_SubC*100)

plot(dat$eloquent, dat$SurgeryExtend.x)

dat_eloquent <- data.frame(dat$SurgeryExtend.x, dat$eloquent)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
eloquent_plot <- ggplot2.histogram(data=dat_eloquent, xName='dat.eloquent',
        groupName='dat.SurgeryExtend.x', binwidth=1, legendPosition="top",
)

ggplot2.customize(eloquent_plot,
                 legendPosition="top",
                 xtitle="Age", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )
# Logistic regression
summary(logisticeloquent <- glm(SurgeryExtend.x ~ eloquent, data=dat, family="binomial"))
tab_model(logisticeloquent) 

