dd <- '/Users/Ivar/Documents/Promotie/Biopsy_prediction/Data/updateMirror'
od <- '/Users/Ivar/Documents/Promotie/Biopsy_prediction/Data/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Biopsy_prediction/Data/R'
setwd(wd)

# import data
data <- read.csv(file.path(dd, 'all_subjects.csv'))
head(data)

#____________________________________________________________________________________________________________________________________#
# Data check & restructure
#____________________________________________________________________________________________________________________________________#


# Number biopsy (create column)
unique(data$SurgeryExtend)
data$biopsy <- (data$SurgeryExtend == "biopsy")
sum(data$biopsy) #345

# Number resection (create column)
data$resection <- (data$SurgeryExtend == "resection")
sum(data$resection) #973

#Total cases in data
nrow(data) #1780

# Exclude NA in biosy or resection and create new DF
data$SurgeryExtend [data$SurgeryExtend == ""] <- NA #rename missing to NA
dat <- data[!is.na(data$SurgeryExtend),]
head(dat)

# Total cases with available outcome data
nrow(dat)#1318


#____________________________________________________________________________________________________________________________________#

# change data class
dat$SurgeryExtend <- as.factor(dat$SurgeryExtend)
dat$GenderV2 <- as.factor(dat$GenderV2)
dat$KPSpre <- as.numeric(dat$KPSpre)
dat$group <- as.factor(dat$group)

dat$ENTside <- as.factor(dat$ENTside)
dat$Multifocal <- as.logical(dat$Multifocal)
dat$Glogical2 <- as.factor(dat$GenderV2)
dat$Sattelites <- as.logical(dat$Sattelites)
dat$nSatteliteLesions <- as.factor(dat$nSatteliteLesions)

class(dat$age) #numeric
class(dat$ENTvolML) #numeric

#___________________________________________________________
# Rescore data
library(dplyr)

# ASA 0-4 -> 1-5 
dat$ASA [dat$ASA == 4] <- 5
dat$ASA [dat$ASA == 3] <- 4
dat$ASA [dat$ASA == 2] <- 3
dat$ASA [dat$ASA == 1] <- 2
dat$ASA [dat$ASA == 0] <- 1

table(dat$ASA)  #1   2   3   4   5 
                #48 106 112  94   1 

# Grouped ASA 1-2 & 3-5
dat$ASA [dat$ASA == "5" |
         dat$ASA == "4" |
         dat$ASA == "3"] <- "High"
dat$ASA [dat$ASA == "2" |
         dat$ASA == "1"] <- "Low"

dat$ASA <- as.factor(dat$ASA)

unique(dat$ASA)

# KPS grouping
# 100-70
# 50-60
# 10-40

unique(dat$KPSpre)

dat$KPSpre [dat$KPSpre >= 7] <- ">=70"
dat$KPSpre [dat$KPSpre == 6 |
            dat$KPSpre == 5] <- "50-70"
dat$KPSpre [dat$KPSpre == 4 |
            dat$KPSpre == 3 |
            dat$KPSpre == 2 |
            dat$KPSpre == 1] <- "<50"

dat$KPSpre <- as.factor(dat$KPSpre)

unique(dat$KPSpre)

# GenderV2
unique(dat$GenderV2)
dat$GenderV2 [dat$GenderV2 == ""] <- NA

# ENTside
unique(dat$ENTside)
dat$ENTside [dat$ENTside == ""] <- NA


#____________________________________________________________________________________________________________________________________#
# Included variables
#____________________________________________________________________________________________________________________________________#

# Patient characteristics
age 
GenderV2
KPSpre
ASA
group

# Tumor characteristics
ENTside
ENTvolML
Multifocal 


#____________________________________________________________________________________________________________________________________#
# Cross tables
#____________________________________________________________________________________________________________________________________#

prop.table(xtabs(~ SurgeryExtend + GenderV2, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend + KPSpre, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend + ASA, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend + group, data=dat), margin = 2)

prop.table(xtabs(~ SurgeryExtend + ENTside, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend + Multifocal, data=dat), margin = 2)

#____________________________________________________________________________________________________________________________________#
# Plots
#____________________________________________________________________________________________________________________________________#


# with easyGgplot2 http://www.sthda.com/english/wiki/ggplot2-histogram-easy-histogram-graph-with-ggplot2-r-package
library(usethis)
library(devtools)
#install_github("kassambara/easyGgplot2")
library(easyGgplot2)

#____________________________________________________________________________________________________________________________________#
## Age


hist(dat$age)

# Build dataset with variables
dat_age <- data.frame(dat$SurgeryExtend, dat$age)
head(dat_age)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
ggplot2.histogram(data=dat_age, xName='dat.age',
        groupName='dat.SurgeryExtend', binwidth=1, legendPosition="top",
)
# Histogram plots with mean lines
ggplot2.histogram(data=dat_age, xName='dat.age',
    groupName='dat.SurgeryExtend', binwidth=1, legendPosition="top",
    alpha=0.5, addDensity=TRUE,
    addMeanLine=TRUE, meanLineColor="white", meanLineSize=1.5
)

# save in od 
#ggsave(
  "ageplot_2.png",
  path = od,
  scale = 1,
)

#____________________________________________________________________________________________________________________________________#
## Volume

hist(dat$ENTvolML)

# Build dataset with variables
dat_volume <- data.frame(dat$SurgeryExtend, dat$ENTvolML)
head(dat_volume)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
ggplot2.histogram(data=dat_volume, xName='dat.ENTvolML',
        groupName='dat.SurgeryExtend', legendPosition="top",)
# Histogram plots with semi-transparent fill.
# alpha is the transparency of the overlaid color
ggplot2.histogram(data=dat_volume, xName='dat.ENTvolML',
    groupName='dat.SurgeryExtend', legendPosition="top",
        alpha=0.5 )
# Histogram plots with mean lines
ggplot2.histogram(data=dat_volume, xName='dat.ENTvolML',
    groupName='dat.SurgeryExtend', legendPosition="top",
    alpha=0.5, addDensity=TRUE,
    addMeanLine=TRUE, meanLineColor="white", meanLineSize=1.5
)

# save in od 
#ggsave(
  "volumeplot.png",
  path = od,
  scale = 1,
)

#____________________________________________________________________________________________________________________________________#
## hospital

#create table
hospital <- prop.table(xtabs(~ SurgeryExtend + group, data=dat), margin = 2)

write.csv(hospital, file.path(od, "hospital_percentages.csv"))

png(file.path(od,"hospital_percentages.jpg"), width = 700, height = 400)
barplot(hospital, main="surgery by hospital",
  xlab="hospital", col=c("darkblue", "red"),
  legend = rownames(hospital)
)
dev.off()


# https://www.r-graph-gallery.com/48-grouped-barplot-with-ggplot2.html
# with ggplot 2
library(ggplot2)
library(dplyr)
library(tidyr)



#____________________________________________________________________________________________________________________________________#
# Simple Logistic regression
#____________________________________________________________________________________________________________________________________#



# set reference value

dat$GenderV2 <- relevel(dat$GenderV2, ref = "Male")
dat$KPSpre <- relevel(dat$KPSpre, ref = ">=70")
dat$ASA <- relevel(dat$ASA, ref = "Low")
dat$ENTside <- relevel(dat$ENTside, ref = "R")

#age
logisticage <- glm(SurgeryExtend ~ age, data=dat, family="binomial")
summary(logisticage)
#Gender
logisticgender <- glm(SurgeryExtend ~ GenderV2, data=dat, family="binomial")
summary(logisticgender)
#KPS
logisticKPS <- glm(SurgeryExtend ~ KPSpre, data=dat, family="binomial")
summary(logisticKPS)
#ASA
logisticASA <- glm(SurgeryExtend ~ ASA, data=dat, family="binomial")
summary(logisticASA)
#group
logisticgroup <- glm(SurgeryExtend ~ group, data=dat, family="binomial")
summary(logisticgroup)

#Side
logisticside <- glm(SurgeryExtend ~ ENTside, data=dat, family="binomial")
summary(logisticside)
#Size
logisticsize<- glm(SurgeryExtend ~ ENTvolML, data=dat, family="binomial")
summary(logisticsize)
#Multifocal
logisticMultifocal <- glm(SurgeryExtend ~ Multifocal, data=dat, family="binomial")
summary(logisticMultifocal)



# TABLE Odds ratio's simple logistic regression
library(sjPlot)
tab_model(logisticage, logisticgender, logisticKPS, logisticASA, logisticside, logisticsize, logisticMultifocal, show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE)



#____________________________________________________________________________________________________________________________________#
# MULTIVARIABLE LOGISTIC REGRESSION
#____________________________________________________________________________________________________________________________________#

#logistic regression
summary(model1 <- glm(SurgeryExtend ~ age + KPSpre + ENTside + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1159.9 (BEST FIT)
summary(model2 <- glm(SurgeryExtend ~ KPSpre + ENTside + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1174.3
summary(model3 <- glm(SurgeryExtend ~ age + ENTside + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1202.9
summary(model4 <- glm(SurgeryExtend ~ age + KPSpre + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1174.5
summary(model5 <- glm(SurgeryExtend ~ age + KPSpre + ENTside + Multifocal, data=dat, family="binomial")) #AIC 1196.2
summary(model6 <- glm(SurgeryExtend ~ age + KPSpre + ENTside + ENTvolML, data=dat, family="binomial")) #AIC 1200.2


# TABLE Odds ratio's multivariable logistic regression
tab_model(model1)

# check linearity? # https://ourcodingclub.github.io/tutorials/mixed-models/
plot(model1, which=1)
plot(model1, which=2)


#____________________________________________________________________________________________________________________________________#
# MIXED (random) EFFECT MODEL (frailty terms)  
                                                # https://www.youtube.com/watch?v=FCcVPsq8VcA
                                                # https://m-clark.github.io/mixed-models-with-R/random_intercepts.html
                                                # https://ourcodingclub.github.io/tutorials/mixed-models/
library(lme4)
summary(MIXmodel1 <- glmer(SurgeryExtend ~ age + KPSpre + ENTside + ENTvolML + Multifocal + (1|group), data=dat, family="binomial")) #AIC 1138.4
tab_model(MIXmodel1) #§ interpretation? ICC = 0.06, dus 0.06 van de variance wordt verklaart door groepverschillen, dit is erg laag. http://cran.nexr.com/web/packages/sjstats/vignettes/mixedmodels-statistics.html

plot(MIXmodel1)

confint(MIXmodel1)
ranef(MIXmodel1)$group
coef(MIXmodel1)$group

#Plot
library(merTools)
predictInterval(MIXmodel1)
REsim(MIXmodel1)
plotREsim(REsim(MIXmodel1))

#anova(model1, MIXmodel1) #compare models

#____________________________________________________________________________________________________________________________________#
# Interaction terms  https://www.youtube.com/watch?v=oVeMquBjNYs
summary(INTmodel1 <- glm(SurgeryExtend ~ age + KPSpre + ENTside + ENTvolML + Multifocal + age:KPSpre + age:ENTside + age:ENTvolML + age:Multifocal + KPSpre:ENTside + KPSpre:ENTvolML + KPSpre:Multifocal + ENTside:ENTvolML + ENTside:Multifocal + ENTvolML:Multifocal, data=dat, family="binomial"))
tab_model(INTmodel1)



#____________________________________________________________________________________________________________________________________#
# assumption check
library (broom)
library (tidyverse)

# Methode 1____________________________________________________________________________________________________________________________________
# http://www.sthda.com/english/articles/36-classification-methods-essentials/148-logistic-regression-assumptions-and-diagnostics-in-r/#logistic-regression-diagnostics

# Predict the probability (p) of diabete positivity
probabilities <- predict(model1, type = "response")
predicted.classes <- ifelse(probabilities > 0.5, "pos", "neg")
head(predicted.classes)
# Select only numeric predictors
mydata <- dat %>%
  dplyr::select_if(is.numeric) 
predictors <- colnames(mydata)
# Bind the logit and tidying the data for plot
mydata <- mydata %>%
  mutate(logit = log(probabilities/(1-probabilities))) %>% # ERROR
  gather(key = "predictors", value = "predictor.value", -logit)
#Create the scatter plots:
ggplot(mydata, aes(logit, predictor.value))+
  geom_point(size = 0.5, alpha = 0.5) +
  geom_smooth(method = "loess") + 
  theme_bw() + 
  facet_wrap(~predictors, scales = "free_y")

# Methode 2____________________________________________________________________________________________________________________________________
# https://www.youtube.com/watch?v=5TW_wWeTOe0
summary(dat$age)
   #Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
  #18.00   53.90   63.30   61.49   70.70   89.50      83 

prop.table(xtabs(~ dat$SurgeryExtend[age < 53.9] , data=dat), margin = 1)

table((dat$SurgeryExtend[dat$age < 53.9]))

P1 <- mean(table(dat$SurgeryExtend[dat$age < 53.9]))
P2 <- mean(table(dat$SurgeryExtend[dat$age > 53.9 & dat$age < 63.3]))
P3 <- mean(table(dat$SurgeryExtend[dat$age > 63.3 & dat$age < 70.7]))
P4 <- mean(table(dat$SurgeryExtend[dat$age > 70.7]))


