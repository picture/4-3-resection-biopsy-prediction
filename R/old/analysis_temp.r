dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data/updateMirror'
od <- '/Users/Ivar/Documents/Promotie/Biopsy_prediction/Data/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Biopsy_prediction/Data/R'
setwd(wd)

# import data
data <- read.csv(file.path(dd, 'all_subjects.csv'))
head(data)

#____________________________________________________________________________________________________________________________________#
# Data check & restructure
#____________________________________________________________________________________________________________________________________#


# Number biopsy (create column)
unique(data$SurgeryExtend.x)
data$biopsy <- (data$SurgeryExtend.x == "biopsy")
sum(data$biopsy) #345

# Number resection (create column)
data$resection <- (data$SurgeryExtend.x == "resection")
sum(data$resection) #973

#Total cases in data
nrow(data) #1780

# Exclude NA in biosy or resection and create new DF
data$SurgeryExtend.x [data$SurgeryExtend.x == ""] <- NA #rename missing to NA
dat <- data[!is.na(data$SurgeryExtend.x),]
head(dat)

# Total cases with available outcome data
nrow(dat)#1318


#____________________________________________________________________________________________________________________________________#

# change data class
dat$SurgeryExtend.x <- as.factor(dat$SurgeryExtend.x)
dat$GenderV2.x <- as.factor(dat$GenderV2.x)
dat$KPSpre.x <- as.numeric(dat$KPSpre.x)
dat$group.x <- as.factor(dat$group.x)

class(dat$age.x.x) #numeric
class(dat$ASA.x.x)

#___________________________________________________________
# Rescore data
library(dplyr)

# ASA.x 0-4 -> 1-5 
dat$ASA.x [dat$ASA.x == 4] <- 5
dat$ASA.x [dat$ASA.x == 3] <- 4
dat$ASA.x [dat$ASA.x == 2] <- 3
dat$ASA.x [dat$ASA.x == 1] <- 2
dat$ASA.x [dat$ASA.x == 0] <- 1

table(dat$ASA.x)  #1   2   3   4   5 
                #48 106 112  94   1 

# Grouped ASA.x 1-2 & 3-5
dat$ASA.x [dat$ASA.x == "5" |
         dat$ASA.x == "4" |
         dat$ASA.x == "3"] <- "High"
dat$ASA.x [dat$ASA.x == "2" |
         dat$ASA.x == "1"] <- "Low"

dat$ASA.x <- as.factor(dat$ASA.x)

unique(dat$ASA.x)

# KPS grouping
# 100-70
# 50-60
# 10-40

unique(dat$KPSpre.x)

dat$KPSpre.x [dat$KPSpre.x >= 7] <- ">=70"
dat$KPSpre.x [dat$KPSpre.x == 6 |
            dat$KPSpre.x == 5] <- "50-70"
dat$KPSpre.x [dat$KPSpre.x == 4 |
            dat$KPSpre.x == 3 |
            dat$KPSpre.x == 2 |
            dat$KPSpre.x == 1] <- "<50"

dat$KPSpre.x <- as.factor(dat$KPSpre.x)

unique(dat$KPSpre.x)

# GenderV2.x
unique(dat$GenderV2.x)
dat$GenderV2.x [dat$GenderV2.x == ""] <- NA

# ENTside
unique(dat$ENTside)
dat$ENTside [dat$ENTside == ""] <- NA


#____________________________________________________________________________________________________________________________________#
# Included variables
#____________________________________________________________________________________________________________________________________#

# Patient characteristics
age.x 
GenderV2.x
KPSpre.x
ASA.x
group.x

# Tumor characteristics
ENTside
ENTvolML
Multifocal 


#____________________________________________________________________________________________________________________________________#
# Cross tables
#____________________________________________________________________________________________________________________________________#

prop.table(xtabs(~ SurgeryExtend.x + GenderV2.x, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + KPSpre.x, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + ASA.x, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + group.x, data=dat), margin = 2)

prop.table(xtabs(~ SurgeryExtend.x + ENTside, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + Multifocal, data=dat), margin = 2)

#____________________________________________________________________________________________________________________________________#
# Plots
#____________________________________________________________________________________________________________________________________#


# with easyGgplot2 http://www.sthda.com/english/wiki/ggplot2-histogram-easy-histogram-graph-with-ggplot2-r-package
library(usethis)
library(devtools)
#install_github("kassambara/easyGgplot2")
library(easyGgplot2)

#____________________________________________________________________________________________________________________________________#
## Age


hist(dat$age.x)

# Build dataset with variables
dat_age <- data.frame(dat$SurgeryExtend.x, dat$age.x)
head(dat_age)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
ggplot2.histogram(data=dat_age, xName='dat.age.x',
        groupName='dat.SurgeryExtend.x', binwidth=1, legendPosition="top",
)
# Histogram plots with mean lines
ggplot2.histogram(data=dat_age, xName='dat.age.x',
    groupName='dat.SurgeryExtend.x', binwidth=1, legendPosition="top",
    alpha=0.5, addDensity=TRUE,
    addMeanLine=TRUE, meanLineColor="white", meanLineSize=1.5
)

# save in od 
#ggsave(
  "ageplot.png",
  path = od,
  scale = 1,
)

#____________________________________________________________________________________________________________________________________#
## Volume

hist(dat$ENTvolML)

# Build dataset with variables
dat_volume <- data.frame(dat$SurgeryExtend.x, dat$ENTvolML)
head(dat_volume)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
ggplot2.histogram(data=dat_volume, xName='dat.ENTvolML',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)
# Histogram plots with semi-transparent fill.
# alpha is the transparency of the overlaid color
ggplot2.histogram(data=dat_volume, xName='dat.ENTvolML',
    groupName='dat.SurgeryExtend.x', legendPosition="top",
        alpha=0.5 )
# Histogram plots with mean lines
ggplot2.histogram(data=dat_volume, xName='dat.ENTvolML',
    groupName='dat.SurgeryExtend.x', legendPosition="top",
    alpha=0.5, addDensity=TRUE,
    addMeanLine=TRUE, meanLineColor="white", meanLineSize=1.5
)

# save in od 
#ggsave(
  "volumeplot.png",
  path = od,
  scale = 1,
)

#____________________________________________________________________________________________________________________________________#
## hospital

#create table
hospital <- prop.table(xtabs(~ SurgeryExtend.x + group.x, data=dat), margin = 2)

write.csv(hospital, file.path(od, "hospital_percentages.csv"))

png(file.path(od,"hospital_percentages.jpg"), width = 700, height = 400)
barplot(hospital, main="surgery by hospital",
  xlab="hospital", col=c("darkblue", "red"),
  legend = rownames(hospital)
)
dev.off()


# https://www.r-graph-gallery.com/48-grouped-barplot-with-ggplot2.html
# with ggplot 2
library(ggplot2)
library(dplyr)
library(tidyr)



#____________________________________________________________________________________________________________________________________#
# Simple Logistic regression
#____________________________________________________________________________________________________________________________________#



# set reference value

dat$GenderV2.x <- relevel(dat$GenderV2.x, ref = "Male")
dat$KPSpre.x <- relevel(dat$KPSpre.x, ref = ">=70")
dat$ASA.x <- relevel(dat$ASA.x, ref = "Low")
dat$ENTside <- relevel(dat$ENTside, ref = "R")

#age.x
logisticage <- glm(SurgeryExtend.x ~ age.x, data=dat, family="binomial")
summary(logisticage)
#Gender
logisticgender <- glm(SurgeryExtend.x ~ GenderV2.x, data=dat, family="binomial")
summary(logisticgender)
#KPS
logisticKPS <- glm(SurgeryExtend.x ~ KPSpre.x, data=dat, family="binomial")
summary(logisticKPS)
#ASA.x
logisticASA <- glm(SurgeryExtend.x ~ ASA.x, data=dat, family="binomial")
summary(logisticASA)
#group.x
logisticgroup <- glm(SurgeryExtend.x ~ group.x, data=dat, family="binomial")
summary(logisticgroup)

#Side
logisticside <- glm(SurgeryExtend.x ~ ENTside, data=dat, family="binomial")
summary(logisticside)
#Size
logisticsize<- glm(SurgeryExtend.x ~ ENTvolML, data=dat, family="binomial")
summary(logisticsize)
#Multifocal
logisticMultifocal <- glm(SurgeryExtend.x ~ Multifocal, data=dat, family="binomial")
summary(logisticMultifocal)



# TABLE Odds ratio's simple logistic regression
library(sjPlot)
tab_model(logisticage, logisticgender, logisticKPS, logisticASA, logisticside, logisticsize, logisticMultifocal, show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE)



#____________________________________________________________________________________________________________________________________#
# MULTIVARIABLE LOGISTIC REGRESSION
#____________________________________________________________________________________________________________________________________#

#logistic regression
summary(model1 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1159.9 (BEST FIT)
summary(model2 <- glm(SurgeryExtend.x ~ KPSpre.x + ENTside + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1174.3
summary(model3 <- glm(SurgeryExtend.x ~ age.x + ENTside + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1202.9
summary(model4 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + ENTvolML + Multifocal, data=dat, family="binomial")) #AIC 1174.5
summary(model5 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + Multifocal, data=dat, family="binomial")) #AIC 1196.2
summary(model6 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + ENTvolML, data=dat, family="binomial")) #AIC 1200.2


# TABLE Odds ratio's multivariable logistic regression
tab_model(model1)

# check linearity? # https://ourcodingclub.github.io/tutorials/mixed-models/
plot(model1, which=1)
plot(model1, which=2)


#____________________________________________________________________________________________________________________________________#
# MIXED (random) EFFECT MODEL (frailty terms)  
                                                # https://www.youtube.com/watch?v=FCcVPsq8VcA
                                                # https://m-clark.github.io/mixed-models-with-R/random_intercepts.html
                                                # https://ourcodingclub.github.io/tutorials/mixed-models/
library(lme4)
summary(MIXmodel1 <- glmer(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + ENTvolML + Multifocal + (1|group.x), data=dat, family="binomial")) #AIC 1138.4
tab_model(MIXmodel1) #§ interpretation? ICC = 0.06, dus 0.06 van de variance wordt verklaart door groepverschillen, dit is erg laag. http://cran.nexr.com/web/packages/sjstats/vignettes/mixedmodels-statistics.html

plot(MIXmodel1)

confint(MIXmodel1)
ranef(MIXmodel1)$group.x
coef(MIXmodel1)$group.x

#Plot
library(merTools)
predictInterval(MIXmodel1)
REsim(MIXmodel1)
plotREsim(REsim(MIXmodel1))

#anova(model1, MIXmodel1) #compare models

#____________________________________________________________________________________________________________________________________#
# Interaction terms  https://www.youtube.com/watch?v=oVeMquBjNYs
summary(INTmodel1 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + ENTvolML + Multifocal + age.x:KPSpre.x + age.x:ENTside + age.x:ENTvolML + age.x:Multifocal + KPSpre.x:ENTside + KPSpre.x:ENTvolML + KPSpre.x:Multifocal + ENTside:ENTvolML + ENTside:Multifocal + ENTvolML:Multifocal, data=dat, family="binomial"))
tab_model(INTmodel1)



#____________________________________________________________________________________________________________________________________#
# assumption check
library (broom)
library (tidyverse)

# Methode 1____________________________________________________________________________________________________________________________________
# http://www.sthda.com/english/articles/36-classification-methods-essentials/148-logistic-regression-assumptions-and-diagnostics-in-r/#logistic-regression-diagnostics

# Predict the probability (p) of ...
probabilities <- predict(model1, type = "response")
predicted.classes <- ifelse(probabilities > 0.5, "pos", "neg")
head(predicted.classes)
# Select only numeric predictors
mydata <- dat %>%
  dplyr::select_if(is.numeric) 
predictors <- colnames(mydata)
# Bind the logit and tidying the data for plot
mydata <- mydata %>%
  mutate(logit = log(probabilities/(1-probabilities))) %>% # ERROR
  gather(key = "predictors", value = "predictor.value", -logit)
#Create the scatter plots:
ggplot(mydata, aes(logit, predictor.value))+
  geom_point(size = 0.5, alpha = 0.5) +
  geom_smooth(method = "loess") + 
  theme_bw() + 
  facet_wrap(~predictors, scales = "free_y")

# Methode 2____________________________________________________________________________________________________________________________________
# https://www.youtube.com/watch?v=5TW_wWeTOe0
summary(dat$age.x)
   #Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's 
  #18.00   53.90   63.30   61.49   70.70   89.50      83 

prop.table(xtabs(~ dat$SurgeryExtend.x[age.x < 53.9] , data=dat), margin = 1)

table((dat$SurgeryExtend.x[dat$age.x < 53.9]))

P1 <- mean(table(dat$SurgeryExtend.x[dat$age.x < 53.9]))
P2 <- mean(table(dat$SurgeryExtend.x[dat$age.x > 53.9 & dat$age.x < 63.3]))
P3 <- mean(table(dat$SurgeryExtend.x[dat$age.x > 63.3 & dat$age.x < 70.7]))
P4 <- mean(table(dat$SurgeryExtend.x[dat$age.x > 70.7]))


