#____________________________________________________________________________________________________________________________________#
# Data_complete(data) check & restructure
#____________________________________________________________________________________________________________________________________#

dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data'
od <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R'
setwd(wd)

# import data
dat <- read.csv(file.path(dd, 'data_complete.csv'))
head(dat)
nrow(dat) #1134

# Number biopsy (create column)
unique(dat$SurgeryExtend.x)
dat$biopsy <- (dat$SurgeryExtend.x == "biopsy")
sum(dat$biopsy) #314 (345 voor merge)

# Number resection (create column)
dat$resection <- (dat$SurgeryExtend.x == "resection")
sum(dat$resection) #820 (973 voor merge)


#____________________________________________________________________________________________________________________________________#
# Included variables
#____________________________________________________________________________________________________________________________________#

# Clinical
age 
GenderV2
KPSpre
ASA
group


# change data class clinical
dat$SurgeryExtend.x <- as.factor(dat$SurgeryExtend.x)
dat$GenderV2.x <- as.factor(dat$GenderV2.x)
dat$KPSpre.x <- as.numeric(dat$KPSpre.x)
dat$group.x <- as.factor(dat$group.x)

class(dat$age.x) #numeric
class(dat$ASA.x) #integer

# Imaging
dat$vol <- dat$Original_volume_Main.x
dat$multif <- dat$Mutifocality
dat$left <- dat$Left_laterality_Main
dat$right <- dat$Right_laterality_Main
dat$RI <- dat$ResectionIndex_Main
dat$billat <- dat$Midline_crossing_Main

# change data class imaging
dat$vol <- as.numeric(dat$vol)
dat$multif <- as.logical(dat$multif)
dat$SurgeryExtend.x <- as.factor(dat$SurgeryExtend.x)
dat$billat <- as.logical(dat$billat)

class(dat$ResVol) #numeric
class(dat$left) #numeric
class(dat$RI) #numeric

#___________________________________________________________

# Rescore data
library(dplyr)

# ASA.x 0-4 -> 1-5 
dat$ASA.x [dat$ASA.x == 4] <- 5
dat$ASA.x [dat$ASA.x == 3] <- 4
dat$ASA.x [dat$ASA.x == 2] <- 3
dat$ASA.x [dat$ASA.x == 1] <- 2
dat$ASA.x [dat$ASA.x == 0] <- 1
table(dat$ASA.x)  #1   2   3   4   5 
                  #45  63  96  87  0 


# Grouped ASA.x 
# 1-2 & 3-5
dat$ASA.x [dat$ASA.x == "5" |
         dat$ASA.x == "4" |
         dat$ASA.x == "3"] <- "High"
dat$ASA.x [dat$ASA.x == "2" |
         dat$ASA.x == "1"] <- "Low"
dat$ASA.x <- as.factor(dat$ASA.x)
unique(dat$ASA.x)


# KPS grouping
# 100-70
# 50-60
# 10-40
dat$KPSpre.x [dat$KPSpre.x >= 7] <- ">=70"
dat$KPSpre.x [dat$KPSpre.x == 6 |
            dat$KPSpre.x == 5] <- "50-70"
dat$KPSpre.x [dat$KPSpre.x == 4 |
            dat$KPSpre.x == 3 |
            dat$KPSpre.x == 2 |
            dat$KPSpre.x == 1] <- "<50"
dat$KPSpre.x <- as.factor(dat$KPSpre.x)
unique(dat$KPSpre.x)


# dealing with missing values

# GenderV2.x
unique(dat$GenderV2.x)
dat$GenderV2.x [dat$GenderV2.x == ""] <- NA
# RI
dat$RI [dat$RI == "-1"] <- NA
# left
dat$left [dat$left == "-1"] <- NA


# binarize laterality
dat$lat <- dat$left
dat$lat [dat$lat < 50] <- "R"
dat$lat [dat$lat >50 & dat$lat != "R"] <- "L"
dat$lat [dat$lat == 100] <- "L"
unique(dat$lat)
dat$lat <- as.factor(dat$lat)
class(dat$lat)

#____________________________________________________________________________________________________________________________________#
# Cross tables
#____________________________________________________________________________________________________________________________________#

# clinical
prop.table(xtabs(~ SurgeryExtend.x + GenderV2.x, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + KPSpre.x, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + ASA.x, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + group.x, data=dat), margin = 2)
# imaging
prop.table(xtabs(~ SurgeryExtend.x + multif, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + lat, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + billat, data=dat), margin = 2)

# clinical
xtabs(~ SurgeryExtend.x + GenderV2.x, data=dat)
xtabs(~ SurgeryExtend.x + KPSpre.x, data=dat)
xtabs(~ SurgeryExtend.x + ASA.x, data=dat)
xtabs(~ SurgeryExtend.x + group.x, data=dat)
# imaging
xtabs(~ SurgeryExtend.x + multif, data=dat)
xtabs(~ SurgeryExtend.x + lat, data=dat)
xtabs(~ SurgeryExtend.x + billat, data=dat)

#____________________________________________________________________________________________________________________________________#
# Plots
#____________________________________________________________________________________________________________________________________#


# with easyGgplot2 http://www.sthda.com/english/wiki/ggplot2-histogram-easy-histogram-graph-with-ggplot2-r-package
library(usethis)
library(devtools)
#install_github("kassambara/easyGgplot2")
library(easyGgplot2)

#____________________________________________________________________________________________________________________________________#
## hospital

#create table
hospital <- prop.table(xtabs(~ SurgeryExtend.x + group.x, data=dat), margin = 2)

write.csv(hospital, file.path(od, "hospital_percentages.csv"))

png(file.path(od,"hospital_percentages.jpg"), width = 700, height = 400)
barplot(hospital, main="surgery by hospital",
  xlab="hospital", col=c("darkblue", "red"),
  legend = rownames(hospital)
)
dev.off()

#____________________________________________________________________________________________________________________________________#
## Age


hist(dat$age.x)

# Build dataset with variables
dat_age <- data.frame(dat$SurgeryExtend.x, dat$age.x)
head(dat_age)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
age_plot <- ggplot2.histogram(data=dat_age, xName='dat.age.x',
        groupName='dat.SurgeryExtend.x', binwidth=1, legendPosition="top",
)

ggplot2.customize(age_plot,
                 legendPosition="top",
                 xtitle="Age", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# save in od 
#ggsave(
  "ageplot.png",
  path = od,
  scale = 1,
)



#________________________________________________________________________________________________________
# Plot Volume 

# Build dataset with variables
dat_vol <- data.frame(dat$SurgeryExtend.x, dat$vol)
head(dat_vol)

# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
vol_plot <- ggplot2.histogram(data=dat_vol, xName='dat.vol',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(vol_plot,
                 legendPosition="top",
                 xtitle="Volume (mL)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# save in od 
#ggsave(
  "volplot_AUT.png",
  path = od,
  scale = 1,
)


#________________________________________________________________________________________________________
# Plot resection index

 # Build dataset with variables
dat_RI <- data.frame(dat$SurgeryExtend.x, dat$RI)
head(dat_RI)

# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
RI_plot <- ggplot2.histogram(data=dat_RI, xName='dat.RI',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(RI_plot,
                 legendPosition="top",
                 xtitle="Resectability Index", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# save in od 
#ggsave(
  "RIplot_AUT.png",
  path = od,
  scale = 1,
)

#________________________________________________________________________________________________________
# Plot Residual Volume DON't INCLUDE

# Build dataset with variables
dat_ResVol <- data.frame(dat$SurgeryExtend.x, dat$ResVol)
head(dat_ResVol)

# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
ResVol_plot <- ggplot2.histogram(data=dat_ResVol, xName='dat.ResVol',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(ResVol_plot,
                 legendPosition="top",
                 xtitle="Residual volume (mL)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# save in od 
#ggsave(
  "ResVolplot_AUT.png",
  path = od,
  scale = 1,
)



#____________________________________________________________________________________________________________________________________#
# Simple Logistic regression
#____________________________________________________________________________________________________________________________________#
# set reference value

dat$GenderV2.x <- relevel(dat$GenderV2.x, ref = "Male")
dat$KPSpre.x <- relevel(dat$KPSpre.x, ref = ">=70")
dat$ASA.x <- relevel(dat$ASA.x, ref = "Low")
dat$lat <- relevel(dat$lat, ref = "R")

# rescore RI to percentage
library(scales)
dat$RI_p <- percent(dat$RI)

#CLINICAL
#age.x
summary(logisticage <- glm(SurgeryExtend.x ~ age.x, data=dat, family="binomial"))
#Gender
summary(logisticgender <- glm(SurgeryExtend.x ~ GenderV2.x, data=dat, family="binomial"))
#KPS
summary(logisticKPS <- glm(SurgeryExtend.x ~ KPSpre.x, data=dat, family="binomial"))
#ASA.x
summary(logisticASA <- glm(SurgeryExtend.x ~ ASA.x, data=dat, family="binomial"))
#group.x
summary(logisticgroup <- glm(SurgeryExtend.x ~ group.x, data=dat, family="binomial"))

#IMAGING
#volume
summary(logisticvol <- glm(SurgeryExtend.x ~ vol, data=dat, family="binomial"))
#multifocality
summary(logisticmultif <- glm(SurgeryExtend.x ~ multif, data=dat, family="binomial"))
#laterality
summary(logisticlat <- glm(SurgeryExtend.x ~ lat, data=dat, family="binomial"))
#Midline crossing
summary(logisticbillat <- glm(SurgeryExtend.x ~ billat, data=dat, family="binomial"))
#resectability Index
summary(logisticRI <- glm(SurgeryExtend.x ~ RI, data=dat, family="binomial"))
#summary(logisticRI_p <- glm(SurgeryExtend.x ~ RI_p, data=dat, family="binomial"))


# TABLE Odds ratio's simple logistic regression
library(sjPlot)
#tab_model(logisticage, logisticgender, logisticKPS, logisticASA, logisticvol, logisticmultif, logisticlat, logisticbillat, logisticRI, show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE)


#____________________________________________________________________________________________________________________________________#
# Extra imaging features

# Cortico Spinal Right Overlap
dat$CST_R <- dat$overlap_Cortico_Spinal_Right_Main
dat$CST_R [dat$CST_R == "-1"] <- NA

# Logistic regression
summary(logisticCST_R <- glm(SurgeryExtend.x ~ CST_R, data=dat, family="binomial"))

#________________________________________________________________________________________________________

# Cortico Spinal Left Overlap
dat$CST_L <- dat$overlap_Cortico_Spinal_Left_Main
dat$CST_L [dat$CST_L == "-1"

# Logistic regression
summary(logisticCST_L <- glm(SurgeryExtend.x ~ CST_L, data=dat, family="binomial"))

#________________________________________________________________________________________________________

# Arcuate Right Overlap
dat$Arc_R <- dat$overlap_Arcuate_Right_Main
dat$Arc_R [dat$Arc_R == "-1"] <- NA

# Logistic regression
summary(logisticArc_R <- glm(SurgeryExtend.x ~ Arc_R, data=dat, family="binomial"))

#________________________________________________________________________________________________________

# Arcuate Left Overlap
dat$Arc_L <- dat$overlap_Arcuate_Left_Main
dat$Arc_L [dat$Arc_L == "-1"] <- NA

# Logistic regression
summary(logisticArc_L <- glm(SurgeryExtend.x ~ Arc_L, data=dat, family="binomial"))

#________________________________________________________________________________________________________
# Right temporal overlap
dat$R_temp <- dat$MNI_temporal_right_Main
dat$R_temp [dat$R_temp == "-1"] <- NA

# Logistic regression
summary(logisticR_temp <- glm(SurgeryExtend.x ~ R_temp, data=dat, family="binomial"))

#________________________________________________________________________________________________________

# Right Occipital overlap
dat$R_Occ <- dat$MNI_occipital_right_Main
dat$R_Occ [dat$R_Occ == "-1"] <- NA

# Logistic regression
summary(logisticR_Occ <- glm(SurgeryExtend.x ~ R_Occ, data=dat, family="binomial"))

#________________________________________________________________________________________________________

# Right frontal overlap
dat$R_Front <- dat$MNI_frontal_right_Main
dat$R_Front [dat$R_Front == "-1"] <- NA

# Logistic regression
summary(logisticR_Front <- glm(SurgeryExtend.x ~ R_Front, data=dat, family="binomial"))
#____________________________________________________________________________________________________________________________________#
# MULTIVARIABLE LOGISTIC REGRESSION
#____________________________________________________________________________________________________________________________________#
summary(model0 <- glm(SurgeryExtend.x ~ 1, data=dat, family="binomial")) #AIC: 1340.1
summary(model1 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + lat + billat + RI, data=dat, family="binomial")) #AIC: 987.62
summary(model2 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + lat + billat + RI + CST_R + CST_L + Arc_L, data=dat, family="binomial")) #AIC: 970.64
summary(model3 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + lat + billat + RI + CST_R + CST_L + Arc_L + R_temp + R_Occ + R_Front, data=dat, family="binomial")) #AIC: 962.52

# TABLE Odds ratio's multivariable logistic regression
tab_model(model1)
tab_model(model2)
tab_model(model3)

# cleaning up the model
# leave KPS out
summary(model4 <- glm(SurgeryExtend.x ~ age.x + vol + multif + lat + billat + RI + CST_R + CST_L + Arc_L + R_temp + R_Occ + R_Front, data=dat, family="binomial")) #AIC: 991.18
# leave out laterality 
summary(model5 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + billat + RI + CST_R + CST_L + Arc_L + R_temp + R_Occ + R_Front, data=dat, family="binomial")) #AIC: 961.54
# leave out laterality, Arc_L 
summary(model6 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + billat + RI + CST_R + CST_L + R_temp + R_Occ + R_Front, data=dat, family="binomial")) #AIC: 961.36
# leave out laterality, Arc_L, R_temp 
summary(model7 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + billat + RI + CST_R + CST_L + R_Occ + R_Front, data=dat, family="binomial")) #AIC: 959.44
# leave out laterality, Arc_L, R_temp, R_Occ
summary(model8 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + billat + RI + CST_R + CST_L + R_Front, data=dat, family="binomial")) #AIC: 968.09

# TABLE Odds ratio's best model (AIC)
tab_model(model7)


#____________________________________________________________________________________________________________________________________#
# automatic glm model selection #
#____________________________________________________________________________________________________________________________________#

library(MASS)
?stepAIC()
stepAIC(model3,
      scope = list(upper = ~ age.x + KPSpre.x + vol + multif + lat + billat + RI + CST_R + CST_L + Arc_L + R_temp + R_Occ + R_Front, lower = ~1),
      trace = FALSE)  #Komt tot zelfde model (model7)

#____________________________________________________________________________________________________________________________________#
# Prediction model #
#____________________________________________________________________________________________________________________________________#

#https://www.youtube.com/watch?v=ZkxyjL8SN_o

# Split into training en test dataset
#set.seed(1)
train <- sample(1:nrow(dat), 0.8 * nrow(dat))
test <- dat[-train,]

# Find best model on training data
stepAIC(model3_train,
      trace = FALSE)
#outcome:
summary(best_model_train <- glm(formula = SurgeryExtend.x ~ age.x + KPSpre.x + vol + multif + 
    billat + RI + CST_R + CST_L + R_Occ + R_Front, family = "binomial", 
    data = dat[train, ])) #AIC: 778.76

# Create prediction model
SurgPred <- predict(best_model_train,newdata = test, type = "respons")

  # Round probability of to "0" or "1", > or < 0,5
p_resection = round(SurgPred)

# Change test data outcome from "biopsy" and "resection" to "0" and "1"
unique(test$SurgeryExtend.x)
class(test$SurgeryExtend.x)
test$SurgeryExtend.x <- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

# change to factors for confusion matrix
p_resection <- as.factor(p_resection)
test$SurgeryExtend.x <- as.factor(test$SurgeryExtend.x)

# make confusion matrix
library(caret)
confusionMatrix(p_resection, test$SurgeryExtend.x)
predictionmodel_results <- confusionMatrix(p_resection, test$SurgeryExtend.x) 

capture.output(confusionMatrix(p_resection, test$SurgeryExtend.x), file.path(od, "predictionmodel_results.csv"))

#as.table(predictionmodel_results)
#results1 <- as.matrix(predictionmodel_results, what = "overall")
#results2 <- as.matrix(predictionmodel_results, what = "classes")
#write.csv(results1, file.path(od, "results_1_confusion matrix.csv"))
#write.csv(results2, file.path(od, "results_2_confusion matrix.csv"))

#____________________________________________________________________________________________________________________________________#
# ROC en AUC graphs
# https://www.youtube.com/watch?v=qcvAqAH60Yw

SurgPred<- as.numeric(SurgPred)

# change NA into 0,5
SurgPred[is.na(SurgPred)] <- 0.5

#Change test data into numeric "0" and "1"
test$SurgeryExtend.x<- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

library(pROC)
library(randomForest)

plot(SurgPred, test$SurgeryExtend.x,)
glm.fit=glm(test$SurgeryExtend.x ~ SurgPred, family = binomial)

length(test$SurgeryExtend.x)
length(SurgPred)
length(glm.fit$fitted.values)

lines(SurgPred, glm.fit$fitted.values)
roc(test$SurgeryExtend.x, glm.fit$fitted.values, plot=TRUE, legacy.axes=TRUE, col='#377eb8', lwd=4, print.auc=TRUE)

# Add Random Forest model
rf.model <- randomForest(factor(test$SurgeryExtend.x) ~ SurgPred)
plot.roc(test$SurgeryExtend.x, rf.model$votes[,1], legacy.axes=TRUE, col="#4daf4a", print.auc=TRUE, add=TRUE, print.auc.y=0.4)
legend("bottomright", legend=c("Logistic Regression", "Random Forest"), col=c('#377eb8',"#4daf4a" ), lwd=4, text.width=0.5)

#--------------------------------------
# gerommel


class(p_resection)
class(test$SurgeryExtend.x)
unique(p_resection)
unique(test$SurgeryExtend.x)

library(predtools)
library(magrittr)
library(dplyr)
library(ggplot2)

?calibration_plot()
calibration_plot(data = SurgPred, obs = "y", pred = "SurgPred")

, y_lim = c(0, 1),
                 title = "Calibration plot for validation data", group = "Surgery")





SurgPred [SurgPred>0.5]<-1
SurgPred [SurgPred<0.5]<-0





fitted_model7_train <- fitted.values(model7_train)


library(gmodels)
CrossTable(x=test$SurgeryExtend.x, y = SurgPred, prop.chisq=FALSE)


library(class)
SurgPred2 <- knn(train = dat[train, ], test = dat[-train,], )



#____________________________________________________________________________________________________________________________________#
### NOG AANPASSEN ###
#____________________________________________________________________________________________________________________________________#

#____________________________________________________________________________________________________________________________________#
# MIXED (random) EFFECT MODEL (frailty terms)  
                                                # https://www.youtube.com/watch?v=FCcVPsq8VcA
                                                # https://m-clark.github.io/mixed-models-with-R/random_intercepts.html
                                                # https://ourcodingclub.github.io/tutorials/mixed-models/
library(lme4)
summary(MIXmodel1 <- glmer(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + ENTvolML + Multifocal + (1|group.x), data=dat, family="binomial")) #AIC 1138.4
tab_model(MIXmodel1) #§ interpretation? ICC = 0.06, dus 0.06 van de variance wordt verklaart door groepverschillen, dit is erg laag. http://cran.nexr.com/web/packages/sjstats/vignettes/mixedmodels-statistics.html

plot(MIXmodel1)

confint(MIXmodel1)
ranef(MIXmodel1)$group.x
coef(MIXmodel1)$group.x

#Plot
library(merTools)
predictInterval(MIXmodel1)
REsim(MIXmodel1)
plotREsim(REsim(MIXmodel1))

#anova(model1, MIXmodel1) #compare models

#____________________________________________________________________________________________________________________________________#
# Interaction terms  https://www.youtube.com/watch?v=oVeMquBjNYs
summary(INTmodel1 <- glm(SurgeryExtend.x ~ age.x + KPSpre.x + ENTside + ENTvolML + Multifocal + age.x:KPSpre.x + age.x:ENTside + age.x:ENTvolML + age.x:Multifocal + KPSpre.x:ENTside + KPSpre.x:ENTvolML + KPSpre.x:Multifocal + ENTside:ENTvolML + ENTside:Multifocal + ENTvolML:Multifocal, data=dat, family="binomial"))
tab_model(INTmodel1)





#____________________________________________________________________________________________________________________________________#
# Prediction model age only#
#____________________________________________________________________________________________________________________________________#

#https://www.youtube.com/watch?v=ZkxyjL8SN_o

# Split into training en test dataset
#set.seed(1)
train <- sample(1:nrow(dat), 0.8 * nrow(dat))
test <- dat[-train,]

Model_vol <- glm(SurgeryExtend.x ~ vol, data=dat[train,], family="binomial")

# Create prediction model
SurgPred_vol <- predict(Model_vol,newdata = test, type = "respons") #ALLES TUSSEN 0.6 en 0.8

  # Round probability of to "0" or "1", > or < 0,5
p_resection_vol = round(SurgPred_vol)

# Change test data outcome from "biopsy" and "resection" to "0" and "1"
unique(test$SurgeryExtend.x)
class(test$SurgeryExtend.x)
test$SurgeryExtend.x <- as.numeric(test$SurgeryExtend.x)
test$SurgeryExtend.x [test$SurgeryExtend.x == 1] <- 0
test$SurgeryExtend.x [test$SurgeryExtend.x == 2] <- 1

# change to factors for confusion matrix
p_resection_vol <- as.factor(p_resection_vol)
test$SurgeryExtend.x <- as.factor(test$SurgeryExtend.x)

# make confusion matrix
library(caret)
confusionMatrix(p_resection_vol, test$SurgeryExtend.x)
predictionmodel_results <- confusionMatrix(p_resection_vol, test$SurgeryExtend.x) 