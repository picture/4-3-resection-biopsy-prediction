#____________________________________________________________________________________________________________________________________#
# Data_complete(data) check & restructure
#____________________________________________________________________________________________________________________________________#

dd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/Data'
od <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R/out'
wd <- '/Users/Ivar/Documents/Promotie/Git/4-3-resection-biopsy-prediction/R'
setwd(wd)

# import data
dat <- read.csv(file.path(dd, 'data_complete.csv'))
head(dat)
nrow(dat) #1134

# Number biopsy (create column)
unique(dat$SurgeryExtend.x)
dat$biopsy <- (dat$SurgeryExtend.x == "biopsy")
sum(dat$biopsy) #314 (345 voor merge)

# Number resection (create column)
dat$resection <- (dat$SurgeryExtend.x == "resection")
sum(dat$resection) #820 (973 voor merge)


#____________________________________________________________________________________________________________________________________#
# Included variables
#____________________________________________________________________________________________________________________________________#

# Clinical
age 
GenderV2
KPSpre
ASA
group

# Imaging
dat$vol <- dat$Original_volume_Main.x
dat$multif <- dat$Mutifocality
dat$left <- dat$Left_laterality_Main
dat$right <- dat$Right_laterality_Main
dat$RI <- dat$ResectionIndex_Main
dat$billat <- dat$Midline_crossing_Main


# change data class

dat$vol <- as.numeric(dat$vol)
dat$multif <- as.logical(dat$multif)
dat$SurgeryExtend.x <- as.factor(dat$SurgeryExtend.x)
dat$billat <- as.logical(dat$billat)

# dealing with missing values

dat$RI [dat$RI == "-1"] <- NA
dat$left [dat$left == "-1"] <- NA



#________________________________________________________________________________________________________
# Laterality
#________________________________________________________________________________________________________

# binarize

dat$lat <- dat$left
dat$lat [dat$lat < 50] <- "R"
dat$lat [dat$lat >50 & dat$lat != "R"] <- "L"
dat$lat [dat$lat == 100] <- "L"

unique(dat$lat)
class(dat$lat)


#________________________________________________________________________________________________________
# Crosstabs
#________________________________________________________________________________________________________

prop.table(xtabs(~ SurgeryExtend.x + multif, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + lat, data=dat), margin = 2)
prop.table(xtabs(~ SurgeryExtend.x + billat, data=dat), margin = 2)



#________________________________________________________________________________________________________
# Plots
#________________________________________________________________________________________________________

library(usethis)
library(devtools)
library(easyGgplot2)


#________________________________________________________________________________________________________
# Plot Volume 

# Build dataset with variables
dat_vol <- data.frame(dat$SurgeryExtend.x, dat$vol)
head(dat_vol)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
vol_plot <- ggplot2.histogram(data=dat_vol, xName='dat.vol',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)


ggplot2.customize(vol_plot,
                 legendPosition="top",
                 xtitle="Volume (mL)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )


# save in od 
ggsave(
  "volplot_AUT.png",
  path = od,
  scale = 1,
)


#________________________________________________________________________________________________________
# Plot resection index

 # Build dataset with variables
dat_RI <- data.frame(dat$SurgeryExtend.x, dat$RI)
head(dat_RI)


# Multiple histograms on the same plot
# Color the histogram plot by the groupName "sex"
RI_plot <- ggplot2.histogram(data=dat_RI, xName='dat.RI',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(RI_plot,
                 legendPosition="top",
                 xtitle="Resectability Index", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )


# save in od 
ggsave(
  "RIplot_AUT.png",
  path = od,
  scale = 1,
)



#____________________________________________________________________________________________________________________________________#
# Simple Logistic regression
#____________________________________________________________________________________________________________________________________#


#volume
summary(logisticvol <- glm(SurgeryExtend.x ~ vol, data=dat, family="binomial"))
#multifocality
summary(logisticmultif <- glm(SurgeryExtend.x ~ multif, data=dat, family="binomial"))
#laterality
summary(logisticlat <- glm(SurgeryExtend.x ~ lat, data=dat, family="binomial"))
#Midline crossing
summary(logisticbillat <- glm(SurgeryExtend.x ~ billat, data=dat, family="binomial"))
#resectability Index
summary(logisticRI <- glm(SurgeryExtend.x ~ RI, data=dat, family="binomial"))


library(sjPlot)
tab_model(logisticvol, logisticmultif, logisticlat, logisticbillat, logisticRI, show.intercept = FALSE, show.obs = FALSE, show.r2 = FALSE)



#____________________________________________________________________________________________________________________________________#
# MULTIVARIABLE LOGISTIC REGRESSION
#____________________________________________________________________________________________________________________________________#

#logistic regression
summary(model1 <- glm(SurgeryExtend.x ~ vol + multif + lat + billat + RI, data=dat, family="binomial")) 

tab_model(model1)
