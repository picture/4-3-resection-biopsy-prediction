##____________________________________________________________________________________________________________________________________#
## Plots ##
##____________________________________________________________________________________________________________________________________#


# with easyGgplot2 http://www.sthda.com/english/wiki/ggplot2-histogram-easy-histogram-graph-with-ggplot2-r-package
library(usethis)
library(devtools)
#install_github("kassambara/easyGgplot2")
library(easyGgplot2)

#____________________________________________________________________________________________________________________________________#
## hospital

#create table
hospital <- prop.table(xtabs(~ SurgeryExtend.x + group.x, data=dat), margin = 2)
#write.csv(hospital, file.path(od, "hospital_percentages.csv"))
png(file.path(od,"hospital_percentages.jpg"), width = 700, height = 400)
barplot(hospital, main="surgery by hospital",
  xlab="hospital", col=c("darkblue", "red"),
  legend = rownames(hospital)
)
dev.off()

#____________________________________________________________________________________________________________________________________#
## Age
hist(dat$age.x)

# Build dataset with variables
dat_age <- data.frame(dat$SurgeryExtend.x, dat$age.x)

# Multiple histograms on the same plot
age_plot <- ggplot2.histogram(data=dat_age, xName='dat.age.x',
        groupName='dat.SurgeryExtend.x', binwidth=1, legendPosition="top",)

ggplot2.customize(age_plot,
                 legendPosition="top",
                 xtitle="Age", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# save in od 
#ggsave(
  "ageplot.png",
  path = od,
  scale = 1,)

#________________________________________________________________________________________________________
# Plot Volume 

# Build dataset with variables
dat_vol <- data.frame(dat$SurgeryExtend.x, dat$vol)

# Multiple histograms on the same plot
vol_plot <- ggplot2.histogram(data=dat_vol, xName='dat.vol',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(vol_plot,
                 legendPosition="top",
                 xtitle="Volume (mL)", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# save in od 
#ggsave(
  "volplot_AUT.png",
  path = od,
  scale = 1,
)


#________________________________________________________________________________________________________
# Plot resection index

 # Build dataset with variables
dat_RI <- data.frame(dat$SurgeryExtend.x, dat$RI)

# Multiple histograms on the same plot
RI_plot <- ggplot2.histogram(data=dat_RI, xName='dat.RI',
        groupName='dat.SurgeryExtend.x', legendPosition="top",)

ggplot2.customize(RI_plot,
                 legendPosition="top",
                 xtitle="Resectability Index", ytitle="Number of patients",
                 xtitleFont=c(14, "bold","black"),
                 ytitleFont=c(14, "bold","black"),
                 legendTitle="Surgery",
                 legendTitleFont=c(14, "bold", "black"),
                 legendTextFont=c(14, "plain", "black"),
                 removePanelGrid = TRUE,
                 backgroundColor = "white"
                 )

# save in od 
#ggsave(
  "RIplot_AUT.png",
  path = od,
  scale = 1,
)