4-3-resection-biopsy-prediction

For analysis.r data in folder: UpdateMirror >> all_subjects.csv was used (all XNAT patients)

image_segmentationanalysis.r is analysis of trondheim automatic segmentation data file in folder: Trondheim >> data_complete.csv (merged csv with all_subjects.csv & complete_diagnosis_automatic_segmentations_XNAT.csv & white_matter_features_overall.csv)

For final analysis data_complete_noNA.csv is used. With NA's excluded